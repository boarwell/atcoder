# AtCoder

## テストケース

[ここ](https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAk_SECQ2Nc6SVGii3rHX6Fa?dl=0)にあります。

- ABCのテストケースでディレクトリが切られていないものは、同日開催のARCの中にあります
- ただし、ABC064はないみたい

### ABC -> ARC 対応

- ABC042 -> ARC058
- ABC043 -> ARC059
- ABC044 -> ARC060
- ABC045 -> ARC061
- ABC046 -> ARC062
- ABC047 -> ARC063
- ABC048 -> ARC064
- ABC049 -> ARC065
- ABC050 -> ARC066
- ABC051 -> 独立
- ABC052 -> ARC067
- ABC053 -> ARC068
- ABC054 -> 独立
- ABC055 -> ARC069
- ABC056 -> ARC070
- ABC057 -> 独立
- ABC058 -> ARC071
- ABC059 -> ARC072
- ABC060 -> ARC073
- ABC061 -> 独立
- ABC063 -> ARC075
- ABC064 -> 独立
- ABC065 -> ARC076
- ABC066 -> ARC077
- ABC067 -> ARC078
- ABC068 -> ARC079
- ABC069 -> ARC080
- ABC070 -> 独立
- ABC071 -> ARC081
- ABC072 -> ARC082
- ABC073 -> 不明
- ABC074 -> ARC083
- ABC075 -> 不明
- ABC076 -> 不明
- ABC077 -> ARC084
- ABC078 -> ARC085
- ABC079 -> 不明
- ABC080 -> 不明
- ABC081 -> ARC086
- ABC082 -> ARC087
- ABC083 -> ARC088
- ABC084 -> 不明
- ABC085 -> 不明
- ABC086 -> ARC089
- ABC087 -> ARC090
- ABC088 -> 不明
- ABC089 -> 独立
- ABC090 -> ARC091
- ABC091 -> ARC092
- ABC092 -> ARC093
- ABC094 -> ARC095
- ABC095 -> ARC096
- ABC096 -> 独立

---

[この記事](https://qiita.com/drken/items/fd4e5e3630d0f5859067)に以下のような記述があります。

> （略）問題の傾向がハッキリと固まって来た 2016 年以降の問題を解いていくのがよいと思います。具体的には、ABC 042 ～最新です。

ということでabc042までを解きます。
