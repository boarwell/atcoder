let n, _ = Scanf.scanf "%d %d\n" (fun x y -> x, y)

let mkls i =
  Array.init i (fun _ -> Scanf.scanf "%s\n" (fun s -> s))
  |> Array.to_list

let cat ls =
  let rec aux tmp = function
    | [] -> tmp
    | h :: t -> aux (tmp ^ h) t
  in
  aux "" ls

let () = mkls n |> List.sort String.compare |> cat |> print_endline
