let n, k = Scanf.scanf "%d %d\n" (fun a b -> a, b)
let ds =
  Array.init k (fun _ -> Scanf.scanf "%c " (fun x -> x))
  |> Array.to_list

let rec solve p =
  let s = string_of_int p in
  if List.exists (fun c -> String.contains s c) ds then solve (p + 1)
  else p

let () = solve n |> Printf.printf "%d\n"