let solve n =
  let rec aux i sum = if i = 0 then sum else aux (i - 1) (sum + i) in
  aux n 0

let () = solve (read_int ()) |> Printf.printf "%d\n"