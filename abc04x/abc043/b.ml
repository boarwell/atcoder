let s = read_line ()

let rec solve s ls = 
  let len = String.length s in
  if len = 0 then List.rev ls 
  else
    let sub = (String.sub s 1 (len - 1)) in
    match s.[0] with
    | '0' | '1' -> solve sub (s.[0] :: ls)
    | _ -> solve sub (try List.tl ls with _ -> [])  

let () = List.iter print_char (solve s []) ; print_newline ()