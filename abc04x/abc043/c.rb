n = gets.to_i
as = gets.split.map(&:to_i)
avg, rem = as.inject(:+).divmod(n)

if rem.zero?
  p(as.inject(0) { |pre, x| pre + (x - avg)**2 })
else
  off = as.inject(0) { |pre, x| pre + (x - avg)**2 }
  up = as.inject(0) { |pre, x| pre + (x - (avg + 1))**2 }
  p up < off ? up : off
end
