let solve s =
  let len = String.length s in
  let rec aux i pre' pre =
    if i >= len then if pre' <> pre then (-1, -1) else (i - 1, i)
    else if pre' = s.[i] then (i - 1, i + 1)
    else if pre = s.[i] then (i, i + 2) (*アンバランスにしたいので3文字*)
    else aux (i + 1) pre s.[i]
  in
  aux 2 s.[0] s.[1]

let () =
  let i, j = solve (read_line ()) in
  Printf.printf "%d %d\n" i j