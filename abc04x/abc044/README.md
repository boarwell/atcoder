# ABC044

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAFfV7uSQzQ6A5Semzm8IO3a/ARC060?dl=0

## A - 高橋君とホテルイージー / Tak and Hotels (ABC Edit)

- とくになし

## B - 美しい文字列 / Beautiful Strings

1. Ruby: group_by(&:itself)してall?(size.even?)
2. ソートして頭から見ていく
3. ハッシュテーブルを使って記録

## C - 高橋君とカード / Tak and Cards

- Rubyなら1からNまでcombinationを作ってその平均をAと比べれば簡単にできそう
  - 入力例のN = 33でTLEします

- xのそれぞれを選ぶ/選ばないとして最後まで見ていったときに平均がAになっていたら1を返す再帰関数を作る

- この方針で一応は実装できましたが、メモ化の方法が思いつかずTLEしてしまいました

- 解説を見てもあまりピンときません
  - 三次元配列なの……？

## 振り返り

- ここのところCが完全には解けていません

- 解説を見てもあまりしっくりこなくて先に進んでいます

- 1周目はそれでもいいと思いますが、もし余裕があるなら解説を理解できないなりにも、ほかの方のコードを写経するくらいはしてもいいかもしれません
  - でもわけわからないコードを写経するのってつらいんですよね……
