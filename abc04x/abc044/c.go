package main

import "fmt"

var (
	n, a int
	xs   []int
)

func main() {
	fmt.Scan(&n, &a)
	xs = make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&xs[i])
	}

	fmt.Println(dfs(0, 0, 0))
}

func dfs(i, tmp, nums int) int {
	if i >= len(xs) {
		if nums == 0 {
			return 0
		}

		avg := tmp / nums
		rem := tmp % nums
		if avg == a && rem == 0 {
			return 1
		}
		return 0
	}

	return dfs((i+1), tmp, nums) + dfs((i+1), (tmp+xs[i]), nums+1)
}
