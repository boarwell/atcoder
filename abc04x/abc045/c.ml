(*+を1個しか入れられないです*)
let s = read_int ()

let digits s = int_of_float (log10 (float s) +. 0.5)

let sum_of_digits s =
  let rec aux s' tmp =
    if s' > 0 then aux (s' / 10) (tmp + (s' mod 10)) else tmp
  in
  aux s 0

let pow x n =
  let rec aux i tmp =
    if i = 0 then tmp else aux (i - 1) (tmp * x)
  in
  aux n 1

let rec calc d =
  let base = (pow 10 d) in
  (s / base) + (s mod base)

let rec main d tmp =
  if d >= digits s then tmp
  else main (d + 1) ((calc d) + tmp)

let () = main 0 (sum_of_digits s) |> Printf.printf "%d\n"