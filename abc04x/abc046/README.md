# ABC046

2018-04-03

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAD0FDu2YhBqy5rZvQu6uF2Ua/ARC062?dl=0

## A - AtCoDeerくんとペンキ / AtCoDeer and Paint Cans

- Rubyならsplitしてuniq, sizeすればいい

- OCamlなら地道に比較するしかなさそう
  - List.sort_uniqというのがあった
  - 単純にuniqだけしてくれればいいのに

## B - AtCoDeerくんとボール色塗り / Painting Balls with AtCoDeer

- 組み合わせの問題

- K * (K - 1)^(N-1)でいい？

## C - AtCoDeerくんと選挙速報 / AtCoDeer and Election Report

- なかなか言葉にしづらいけどやりたいことはわかった

- Rubyで一度提出しましたがWAです
  - 解説を見ます

- 解説の通りに実装してもWAが出ます
  - たんに`max(⌈A/x⌉, ⌈B/y⌉)`ではだめっぽいです

- 自分の考え方であっていたっぽい
  - 正しく実装できていれば通っていたっぽい

- 今回の手順
  - 現在の得票数`sumt, suma`を用意
  - `sumt/t`と`suma/a`の大きい方を判定
  - （sumt/tが大きかった場合）
    - sumtがtの倍数のときは解説でいうところのnはsumt/t
    - そうでなければn=sumt/t+1