package main

import "fmt"

var (
	n, k int
)

func main() {
	fmt.Scan(&n, &k)

	fmt.Println(k * exp((k-1), (n-1)))
}

func exp(x, n int) int {
	ans := 1
	for ; n > 0; n-- {
		ans *= x
	}

	return ans
}
