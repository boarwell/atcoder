let () =
  Scanf.scanf "%f %f"
    (fun n k -> int_of_float (k *. (k -. 1.) ** (n -. 1.)))
  |> Printf.printf "%d\n"

