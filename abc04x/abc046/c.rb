n = gets.to_i
sumt = 1
suma = 1

n.times do
  t, a = gets.split.map(&:to_i)

  x = if (suma / a) < (sumt / t)
        (sumt % t).zero? ? (sumt / t) : ((sumt / t) + 1)
      else
        (suma % a).zero? ? (suma / a) : (suma / a) + 1
      end

  sumt = t * x
  suma = a * x
end

p sumt + suma
