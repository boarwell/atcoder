open List

let () =
  Scanf.scanf "%d %d %d" (fun a b c ->
      let ls = sort compare [a; b; c] in
      if nth ls 0 + nth ls 1 = nth ls 2 then "Yes" else "No" )
  |> print_endline

