package main

import "fmt"

var (
	s string
)

func main() {
	fmt.Scan(&s)
	fmt.Println(solve(s) - 1)
}

func solve(s string) int {
	pre := rune(s[0])
	count := 1

	for _, v := range s {
		if pre != v {
			count++
			pre = v
		}
	}

	return count
}
