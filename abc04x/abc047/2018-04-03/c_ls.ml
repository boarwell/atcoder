(*
Rubyのように書いた
stringをリストにばらせないのでString.iterと参照を使って同じようなことをやってます
速度はc.mlと同じでした
*)
let s = read_line ()
let pre = ref s.[0]

let c = ref 0

let () =
  String.iter
    (fun s ->
       if s <> !pre then (
         incr c ;
         pre := s ))
    s ;
  Printf.printf "%d\n" !c

