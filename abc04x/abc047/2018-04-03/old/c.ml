let one_color s =
  not (String.contains s 'W' && String.contains s 'B')

let flip s =
  let len = String.length s in
  let newcolor = if s.[0] = 'W' then 'B' else 'W' in
  (*文字列を頭から見ていって新しい色が出てくるまでカウント*)
  let rec aux i =
    if i = len then String.make len newcolor
    else if s.[i] <> newcolor then aux (i+1)
    (*新しくひとつ置くのでi+1*)
    else (String.make (i+1) newcolor) ^ (String.sub s i (len - i))
  in 
  aux 0

let rec solve s count =
  if one_color s then count
  else solve (flip s) (count + 1)

let () =
  let s = Scanf.scanf "%s" (fun s -> s) in
  Printf.printf "%d\n" (solve s 0)
