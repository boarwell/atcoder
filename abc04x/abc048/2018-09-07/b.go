package main

import "fmt"

var (
	a, b, x int
)

func main() {
	fmt.Scan(&a, &b, &x)
	fmt.Println(f(b, x) - f(a, x))
}

func f(i, j int) int {
	if i == 0 {
		return 0
	}

	return (i / j) + 1
}
