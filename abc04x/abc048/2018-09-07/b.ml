let () =
  Scanf.scanf "%d %d %d" (fun a b x ->
      let start = match a mod x with 0 -> a / x | _ -> a / x + 1 in
      Printf.printf "%d\n" (b / x - start + 1) )

