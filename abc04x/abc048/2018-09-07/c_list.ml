let n, x = Scanf.scanf "%d %d\n" (fun n x -> (n, x))

let a =
  Array.init n (fun _ -> Scanf.scanf "%d " (fun x -> x))
  |> Array.to_list

(*奇数番の数字を調整する -> こっちは必要なかった*)
let rec solve count = function
  | a :: (b :: _ as t) ->
    let sum = a + b in
    if sum > x then solve (count + (sum - x)) t else solve count t
  | [a] -> if a > x then count + (a - x) else count
  | [] -> count


(*偶数番の数字を調整する*)
let rec solve' count = function
  | a :: (b :: c as t) ->
    let sum = a + b in
    if sum > x then solve' (count + (sum - x)) (max 0 (b - (sum - x)) :: c)
    else solve' count t
  | [a] -> if a > x then count + (a - x) else count
  | [] -> count


let () = solve' 0 a |> Printf.printf "%d\n"
