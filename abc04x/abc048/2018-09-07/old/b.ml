let solve a b x =
  let rec aux count n =
    if n > b then count
    else if n mod x = 0 then aux (count + 1) (n+1)
    else aux count (n + 1)
  in
  aux 0 a

let () = Scanf.scanf "%d %d %d" solve |> Printf.printf "%d\n"