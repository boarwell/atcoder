let n, x = Scanf.scanf "%d %d\n" (fun n x -> (n, x))

let a =
  Array.init n (fun _ -> Scanf.scanf "%d " (fun x -> x))
  |> Array.to_list

let rec solve pre count = function
  | [] -> count
  | h :: t ->
    let y = if pre + h > x then (pre + h - x) else 0 in
    solve (h - y) (count + y) t

let () = Printf.printf "%d\n" (solve 0 0 a)