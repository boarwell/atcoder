# https://beta.atcoder.jp/contests/abc048/submissions/1592458
_, x = gets.split.map(&:to_i)
arr = gets.split.map(&:to_i)
ans = 0
prev = 0
arr.each do |i|
  y = prev + i > x ? prev + i - x : 0
  prev = i - y
  ans += y
end
puts ans

