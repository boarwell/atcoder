let () =
  Scanf.scanf "%d %d %d" (fun a b x -> 
      let start = if a mod x = 0 then a / x else a / x + 1 in
      Printf.printf "%d\n" (b / x - start + 1))