let () =
  (match read_line () with
   | "a" | "e" | "i" | "o" | "u" -> "vowel"
   | _ -> "consonant")
  |> print_endline
