ls = %w[a e i o u]
puts (ls.include? gets.chomp) ? 'vowel' : 'consonant'
