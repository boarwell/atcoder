package main

import (
	"fmt"
	"os"
	"strings"
)

var (
	suf = []string{"dream", "dreamer", "erase", "eraser"}
)

func hasAnySuffix(s string) string {
	for _, v := range suf {
		if strings.HasSuffix(s, v) {
			return v
		}
	}

	return ""
}

func main() {
	var s string
	fmt.Scan(&s)

	for len(s) > 0 {
		res := hasAnySuffix(s)
		if res == "" {
			fmt.Println("NO")
			os.Exit(0)
		}
		s = strings.TrimSuffix(s, res)
	}

	fmt.Println("YES")
}
