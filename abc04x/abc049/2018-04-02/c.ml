let words = ["dream"; "dreamer"; "erase"; "eraser"]

let has_suffix s suf =
  let len_s = String.length s in
  let len_suf = String.length suf in
  try String.sub s (len_s - len_suf) len_suf = suf with _ -> false


let has_any_suffix s = try List.find (has_suffix s) words with _ -> ""

let trim_suffix s suf =
  let len_s = String.length s in
  let len_suf = String.length suf in
  String.sub s 0 (len_s - len_suf)


let rec solve s =
  if String.length s = 0 then "YES"
  else
    match has_any_suffix s with
    | "" -> "NO"
    | suf -> trim_suffix s suf |> solve


let () = read_line () |> solve |> print_endline
