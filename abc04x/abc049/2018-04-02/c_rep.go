package main

import (
	"fmt"
	"strings"
)

func main() {
	var s string
	fmt.Scan(&s)

	tmp := strings.Replace(strings.Replace(s, "eraser", "", -1), "erase", "", -1)

	res := strings.Replace(strings.Replace(tmp, "dreamer", "", -1), "dream", "", -1)

	if res == "" {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}

}
