let rev s =
  let len = String.length s in
  let rec aux i acc =
    if i = len then acc else aux (i + 1) (Char.escaped s.[i] ^ acc)
  in
  aux 0 ""


let has_any_prefix s =
  try
    List.find
      (fun word ->
         let len = String.length word in
         try String.sub s 0 len = word with _ -> false)
      ["maerd"; "remaerd"; "esare"; "resare"]
  with Not_found -> ""


let trim_prefix s prefix =
  let len_pre = String.length prefix in
  let len_s = String.length s in
  String.sub s len_pre (len_s - len_pre)


let rec solve str =
  if String.length str = 0 then "YES"
  else
    match has_any_prefix str with
    | "" -> "NO"
    | a -> trim_prefix str a |> solve


let () = read_line () |> rev |> solve |> print_endline
