// https://beta.atcoder.jp/contests/abc049/submissions/2281480
package main

import "fmt"

var (
	s string
	w = [4]string{"maerd", "remaerd", "esare", "resare"}
)

func rev(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}
func main() {
	fmt.Scan(&s)
	s = rev(s)

	flag := true
	for i := 0; i < len(s); {
		flag2 := true

		for j := 0; j < 4; j++ {
			// Goだとこれがないとout of boundsします
			if len(s[i:]) < len(w[j]) {
				break
			}
			if s[i:i+len(w[j])] == w[j] {
				flag2 = false
				i += len(w[j])
				break
			}
		}

		if flag2 {
			flag = false
			break
		}
	}

	if flag {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
