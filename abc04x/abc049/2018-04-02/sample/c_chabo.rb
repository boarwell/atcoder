# https://beta.atcoder.jp/contests/abc049/submissions/2141938
puts gets.chop.gsub("eraser","").gsub("erase","").gsub("dreamer","").gsub("dream","").empty?? :YES: :NO
