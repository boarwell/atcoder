let l = ["dream"; "dreamer"; "erase"; "eraser"]

let has_suffix s suffix =
  let lens = String.length s in
  let len_suf = String.length suffix in
  if lens < len_suf then false
  else String.sub s (lens - len_suf) len_suf = suffix

let trim_suffix s suffix =
  let lens = String.length s in
  let len_suf = String.length suffix in
  String.sub s 0 (lens - len_suf)

let rec f s =
  if s = "" then "YES"
  else
    match List.filter (has_suffix s) l with
    | [] -> "NO"
    | suf -> f (trim_suffix s (List.hd suf))

let () = f (read_line ()) |> print_endline
