let () =
  ignore (read_line ());
  let rec f () =
    match read_line () with
    | s -> (print_endline s; print_endline s); f ()
    | exception _ -> ()
  in
  f ()