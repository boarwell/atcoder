let id x = x

let () =
  let n = Scanf.scanf "%d\n" id in
  let ar = Array.init n (fun _ -> Scanf.scanf "%d " id) in
  let sum = Array.fold_left ( + ) 0 ar in
  let m = Scanf.scanf "%d\n" id in
  let b = Buffer.create (2 * m) in
  for i = 1 to m do
    let ans = Scanf.scanf "%d %d\n"
        (fun p x -> sum - (ar.(p - 1) - x)) in
    string_of_int ans ^ "\n" |> Buffer.add_string b
  done ;
  print_string (Buffer.contents b)

