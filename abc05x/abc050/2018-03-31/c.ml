let id x = x

let n = Scanf.scanf "%d\n" id

let a = Array.init n (fun _ -> Scanf.scanf "%d " id)
        |> Array.to_list |> List.fast_sort compare

let m = 1000000007

let rec f = function
  | [] -> true
  | a :: b :: tl when a = b -> f tl
  | _ -> false


let calc n =
  let rec f i tmp = 
    if i = 0 then tmp else f (i - 1) (2 * tmp mod m)
  in
  f n 1


let () =
  (if f (if n mod 2 == 1 then List.tl a else a) then calc (n / 2) else 0)
  |> Printf.printf "%d\n"

