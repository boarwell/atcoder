# 方向性はこれでいいがTLEでしてしまった

n = gets.to_i
a = gets.split.map(&:to_i)

def valid?(n, a)
  all_valid = true
  if n.even?
    a.sort.uniq.each do |x|
      next if a.count(x) == 2 && x.odd?
      all_valid = false
      break
    end
  else
    a.sort.uniq.each do |x|
      next if (a.count(x) == 2 && x.even?) || (x.zero? && a.count(x) == 1)

      all_valid = false
      break
    end
  end
  all_valid
end

def pow2(n, tmp)
  return (tmp % 1_000_000_007) if n.zero?
  pow2((n - 1), (tmp * 2) % 1_000_000_007)
end

puts valid?(n, a) ? pow2((n / 2), 1) : 0
