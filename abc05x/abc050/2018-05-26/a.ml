let () = 
  Scanf.scanf "%d %c %d" (fun a op b ->
      if op = '+' then a + b else a - b)
  |> Printf.printf "%d\n"