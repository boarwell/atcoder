let id x = x

let n = Scanf.scanf "%d\n" id

let a =
  Array.init n (fun _ -> Scanf.scanf "%d " id)
  |> Array.to_list |> List.fast_sort compare

let m = 1000000007

let rec check = function
  | [] -> true
  | a :: b :: tl when a = b -> check tl
  | _ -> false

let calc n =
  let rec aux i tmp = if i <= 0 then tmp else aux (pred i) (tmp * 2 mod m) in
  aux n 1

let () =
  ( match check (if n mod 2 == 1 then List.tl a else a) with
  | true -> calc (n / 2)
  | false -> 0 )
  |> Printf.printf "%d\n"
