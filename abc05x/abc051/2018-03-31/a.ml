let s = read_line ()

let b = Buffer.create 19

let () =
  String.iter
    (fun c ->
      if c <> ',' then Buffer.add_char b c else Buffer.add_string b " ")
    s ;
  print_endline (Buffer.contents b)

