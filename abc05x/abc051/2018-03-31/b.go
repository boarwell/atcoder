// この方針でもタイムアウトします
package main

import "fmt"

var (
	k, s int
	ans  int
)

func main() {
	fmt.Scan(&k, &s)

	for x := 0; x <= k; x++ {
		for y := x; y <= k; y++ {
			for z := y; z <= k; z++ {
				if x+y+z == s {
					ans++
				}
			}
		}
	}

	if ans == 1 {
		fmt.Println(ans)
	} else {
		fmt.Println(ans * 3)
	}
}
