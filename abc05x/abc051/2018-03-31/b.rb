k, s = gets.split.map(&:to_i)
ans = 0

0.upto(k) do |x|
  0.upto(k) do |y|
    ans += 1 if (s - x - y) >= 0 && (s - x - y) <= k
  end
end
p ans
