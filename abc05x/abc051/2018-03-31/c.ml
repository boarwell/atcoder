let sx, sy, tx, ty = Scanf.scanf "%d %d %d %d" (fun a b c d -> (a, b, c, d))

let x = tx - sx

let y = ty - sy

let x' = succ x

let y' = succ y

let rep s n =
  let rec f i tmp = if i = 1 then tmp else f (i - 1) (s ^ tmp) in
  f n s


let () =
  Printf.printf "%s\n"
    ( rep "R" x ^ rep "U" y ^ rep "L" x ^ rep "D" y ^ "D" ^ rep "R" x'
    ^ rep "U" y' ^ "LU" ^ rep "L" x' ^ rep "D" y' ^ "R" )

