(*https://beta.atcoder.jp/contests/abc051/submissions/2136882*)
open String
let () = Scanf.scanf "%s" (fun s -> Printf.printf "%s %s %s\n" (sub s 0 5) (sub s 6 7) (sub s 14 5))
