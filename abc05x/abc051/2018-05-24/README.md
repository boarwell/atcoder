# ABC051

2018-05-24

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAjpfJgriswOTUWzHeyXIRna/ABC051?dl=0


## A - Haiku

- 文字列を置換する

- OCamlだとどの関数が一番いいのでしょうか
  - String.iterで`,`のときに空白を出力する
  - バッファに書き込む

- 前回とは別のやり方にしようと思います
  - 前回はバッファだったのでiterを使います

## B - Sum of Three Integers

- x, yを二重ループで回す
  - `s - x - y`(z)が0以上k以下である組み合わせの個数を数える

- 前回もforでやっているので、次回は再帰でやってみましょう

## C - Back and Forth

- `sx < tx`, `sy, ty`なので、単純にシミュレートするだけで解けそう

## 振り返り

- 特になし
- 今回は簡単だった
