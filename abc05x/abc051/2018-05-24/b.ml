let ans = ref 0
let k, s = Scanf.scanf "%d %d" (fun a b -> a, b)

let () =
  for x = 0 to k do
    for y = 0 to k do
      let z = s - x - y in
      if 0 <= z && z <= k then incr ans
    done;
  done;
  Printf.printf "%d\n" !ans

