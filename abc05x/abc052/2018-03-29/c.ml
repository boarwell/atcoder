module IntMap = Map.Make(struct type t = int let compare i j = i - j end)

let inc x =
  match x with
  | Some a -> Some (a + 1)
  | None -> Some 1

let n = read_int ()

let rec solve x memo =
  if x = 1 then memo
  else
    let rec prime_fact x' i memo' =
      (if x < i then memo'
       else
         (if x' mod i = 0
          then prime_fact (x' / i) i (IntMap.update i inc memo')
          else prime_fact x' (i + 1) memo'))
    in 
    solve (x - 1) (prime_fact x 2 memo)


let () =
  (IntMap.fold (fun _ v pre -> (v + 1) * pre) (solve n IntMap.empty) 1) mod 100000007
  |> Printf.printf "%d\n"