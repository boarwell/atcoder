h = Hash.new { 0 }
h[1] = 0

2.upto(gets.to_i) do |x|
  2.upto(x) do |i|
    while (x % i).zero?
      x /= i
      h[i] += 1
    end
  end
end

p h.map{ |_k, v| v + 1 }.inject(:*) % (10**9 + 7)