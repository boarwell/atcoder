let mo = 100000007

let h = Hashtbl.create 100

let n = read_int ()

let () = Hashtbl.add h 1 0

let update h' x =
  let v = try (Hashtbl.find h' x) + 1 with _ -> 1 in
  Hashtbl.replace h' x v


let () =
  for x = 2 to n do
    for i = 2 to x do
      let rec f a =
        if a mod i = 0 then (
          update h i ;
          f (a / i) )
      in
      f x
    done
  done ;
  Hashtbl.fold (fun _ v pre -> ((v + 1) mod mo) * (pre mod mo)) h 1 mod mo
  |> Printf.printf "%d\n"

