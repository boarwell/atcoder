(*https://beta.atcoder.jp/contests/abc052/submissions/2139365*)

let arr_of_str s = Array.init (String.length s) (fun i -> s.[i])
let () =
  Scanf.scanf "%d %s" (fun _ s -> arr_of_str s)
  |> Array.fold_left (fun (a,c) o -> if o = 'I' then (max a (c+1), c+1) else (a, c-1)) (0, 0)
  |> fst |> Printf.printf "%d\n"
