let () =
  ignore @@ read_int () ;
  let x = ref 0 in
  let ans = ref 0 in
  String.iter
    (fun c ->
       if c = 'I' then (
         incr x ;
         if !x > !ans then ans := !x )
       else decr x)
    (read_line ()) ;
  Printf.printf "%d\n" !ans

