let s = read_line ()
let () =
  (String.rindex s 'Z') - (String.index s 'A') + 1
  |> Printf.printf "%d\n"