let x = read_int ()

let () =
  (match x mod 11 with
   | 0 -> (x / 11 * 2)
   | n when n <= 6 -> (x / 11) * 2 + 1
   | _ -> (x / 11) * 2 + 2)
  |> Printf.printf "%d\n"