let () =
  let s = read_line () in
  Printf.printf "%d\n" (String.rindex s 'Z' - String.index s 'A' + 1)
