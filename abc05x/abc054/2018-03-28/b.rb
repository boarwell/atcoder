n, m = gets.split.map(&:to_i)
a = n.times.map { gets.chomp.split('') }
b = m.times.map { gets.chomp.split('') }

exist = false

0.upto(n - 1) do |ly|
  0.upto(n - 1) do |lx|
    next if ly + m - 1 >= n || lx + m - 1 >= n

    match = true
    0.upto(m - 1) do |y|
      0.upto(m - 1) do |x|
        match = false if a[ly + y][lx + x] != b[y][x]
      end
    end

    exist = true if match
  end
end

puts exist ? 'Yes' : 'No'
