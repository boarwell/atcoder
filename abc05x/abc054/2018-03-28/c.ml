let scan_words () = Scanf.scanf "%d %d\n" (fun a b -> (a, b))

let n, m = scan_words ()

let h = Hashtbl.create m

let ans = ref 0

let rec search found visited now =
  if List.exists ((=) now) visited then ()
  else if found = n then incr ans
  else
    let found' = found + 1 in
    let visited' = now :: visited in
    match Hashtbl.find_all h now with
    | [] -> ()
    | next_nodes -> List.iter (search found' visited') next_nodes


let () =
  for i = 1 to m do
    let a, b = scan_words () in
    Hashtbl.add h a b ;
    Hashtbl.add h b a
  done ;
  search 1 [] 1 ;
  Printf.printf "%d\n" !ans

