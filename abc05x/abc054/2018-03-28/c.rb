n, m = gets.split.map(&:to_i)
g = Array.new(n) { Array.new(n) { false } }

m.times do
  a, b = gets.split.map { |x| (x.to_i - 1) }
  g[a][b] = g[b][a] = true
end

ans = 0
(1...n).to_a.permutation do |perm|
  puts perm.join(', ')
  ans += 1 if ([0] + perm).each_cons(2).all? { |u, v| g[u][v] }
end

puts ans
