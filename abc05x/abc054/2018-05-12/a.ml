let () =
  (match (Scanf.scanf "%d %d" (fun a b -> a, b)) with
   | a, b when a = b -> "Draw"
   | 1, _ -> "Alice"
   | _, 1 -> "Bob"
   | a, b when a > b -> "Alice"
   | _ -> "Bob")
  |> print_endline