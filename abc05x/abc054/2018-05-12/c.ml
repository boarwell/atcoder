let scan_words () = Scanf.scanf "%d %d\n" (fun a b -> (a, b))

module Is = Set.Make (struct type t = int let compare = compare end)

let h = Hashtbl.create 8
let n, m = scan_words ()

let rec solve now set =
  if Is.mem now set then 0
  (* 現在地ははじめて訪れた && 現在地を含めると訪れた点が頂点数と同じになる *)
  (* -> 現在地が最後の頂点 *)
  else if Is.cardinal set = (n - 1) then 1
  else
    match Hashtbl.find_all h now with
    | [] -> 0
    | ls ->
      List.fold_left (fun acc pos -> acc + solve pos (Is.add now set)) 0 ls

let () =
  for i = 1 to m do
    let a, b = scan_words () in
    Hashtbl.add h a b ; Hashtbl.add h b a
  done ;
  Printf.printf "%d\n" (solve 1 Is.empty)
