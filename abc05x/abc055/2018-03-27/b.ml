let n = read_int ()
let m = int_of_float (10. ** 9. +. 7.)

let fact n =
  let rec f i n' = 
    if i = 1 then n' mod m else f (i - 1) (i mod m * n' mod m)
  in 
  f n 1

let () = Printf.printf "%d\n" (fact n)
