let () =
  Scanf.scanf "%c %c" (fun a b ->
      match a with
      | 'H' -> b
      | _ -> if b = 'H' then 'D' else 'H')
  |> Printf.printf "%c\n"