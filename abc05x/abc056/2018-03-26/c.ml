let n = read_int ()

let rec solve i sum = 
  if n <= sum then i else solve (i + 1) (sum + i + 1)

let () = (solve 1 1) |> Printf.printf "%d\n"