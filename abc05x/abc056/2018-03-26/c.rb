@n = gets.to_i

def solve(i, sum)
  return i if @n <= sum
  solve(i + 1, sum + i + 1)
end

puts(solve(1, 1))