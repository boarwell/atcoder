let () =
  Scanf.scanf "%c %c" (fun a b ->
      if a = 'H' then b else match b with 'H' -> 'D' | _ -> 'H' )
  |> Printf.printf "%c\n"

