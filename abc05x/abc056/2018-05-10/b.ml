let () =
  Scanf.scanf "%d %d %d" (fun w a b ->
      (max a b) - (min a b) - w |> max 0 |> Printf.printf "%d\n")