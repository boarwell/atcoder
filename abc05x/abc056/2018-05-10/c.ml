let solve n =
  let rec aux i sum =
    if sum >= n then i else aux (succ i) (sum + i + 1) in
  aux 1 1

let () = read_int () |> solve |> Printf.printf "%d\n"