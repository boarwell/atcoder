n, m = gets.split.map(&:to_i)
ab = []
cd = []

n.times do
  ab << gets.split.map(&:to_i)
end

m.times do
  cd << gets.split.map(&:to_i)
end

ab.each do |st|
  min = 1 << 31
  index = 0
  cd.each_with_index do |ch, i|
    dist = (st[0] - ch[0]).abs + (st[1] - ch[1]).abs
    if dist < min
      min = dist
      index = (i + 1)
    end
  end
  puts index
end
