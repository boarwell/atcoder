let scan_words () = Scanf.scanf "%d %d\n" (fun a b -> a, b)

let mk_ar (n, m) =
  let rec mk_ab n ls =
    if n <= 0 then ls |> Array.of_list
    else mk_ab (pred n) (scan_words () :: ls)
  in
  let rec mk_cd m ls =
    if m <= 0 then ls |> Array.of_list
    else mk_cd (pred m) (scan_words () :: ls)
  in
  (mk_ab n []), (mk_cd m [])

let solve (ab, cd) =
  let n = Array.length ab in
  let m = Array.length cd in
  let tmp = ref max_int in
  let ans = ref 0 in
  for i = 0 to (n - 1) do 
    tmp := max_int;
    ans := 0;
    for j = 0 to (m - 1) do
      let now = (abs (fst ab.(i) - fst cd.(j))) + (abs (snd ab.(i) - snd cd.(j))) in
      if !tmp > now then (tmp := now; ans := (succ j))
    done;
    Printf.printf "%d\n" !ans
  done;;

let () = mk_ar (scan_words ()) |> solve
