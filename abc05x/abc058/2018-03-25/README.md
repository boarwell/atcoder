# ABC058

2018-03-24

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAfT6_AJRiIzvPmt2_1fLYYa/ARC072?dl=0

## A - ι⊥l

- 問題文にある通りに評価する

## B - ∵∴∵

- バッファを用意して順番に書いていく

## C - 怪文書 / Dubious Document

- 最短の文字列を構成する各文字がその他の文字列に含まれているかを判定する

- 含まれていたらバッファに書き込む

- バッファの文字列を辞書順にソートして出力
  - OCamlで文字列のソートってできるんですかね
  - 標準ライブラリになくても、char型として扱えばソートできそう

- OCaml、Rubyで自分で考えて実装できましたが、解説の方法でもできるようにしておきましょう
  - [この方のコード](https://beta.atcoder.jp/contests/abc058/submissions/1920387)が参考になります

