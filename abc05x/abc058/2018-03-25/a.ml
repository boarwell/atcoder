let () =
  Scanf.scanf "%d %d %d" (fun a b c ->
      begin if (b - a) = (c - b) then "YES" else "NO" end)
  |> print_endline