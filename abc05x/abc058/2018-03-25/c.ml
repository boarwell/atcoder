let n = Scanf.scanf "%d " (fun x -> x)

let ar = Array.init n (fun _ -> Scanf.scanf "%s " (fun s -> s))

let buf = Buffer.create 50

let remove_char s c =
  Str.replace_first (Str.regexp_string (Char.escaped c)) "" s


let string_to_char_list s =
  let rec f i ls =
    if i = String.length s then ls else f (i + 1) (s.[i] :: ls)
  in
  f 0 []

let () =
  Array.fast_sort (fun pre nex -> String.length pre - String.length nex) ar ;
  let shortest = ar.(0) in
  String.iter
    (fun c ->
       let rec search_all_strings i =
         if i = n then true
         else if String.contains ar.(i) c then search_all_strings (i + 1)
         else false
       in
       if search_all_strings 1 then Buffer.add_char buf c ;
       let rec replace_string i =
         if i = n then () else
           (ar.(i) <- remove_char ar.(i) c ; replace_string (i + 1))
       in
       replace_string 1)
    shortest ;
  Buffer.contents buf |> string_to_char_list |> List.sort compare 
  |> List.iter print_char ;
  print_newline ()

