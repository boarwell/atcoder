let o = read_line ()
let e = read_line ()

let leno = String.length o
let lene = String.length e

let solve n =
  let b = Buffer.create n in
  let rec aux i =
    if i >= n then Buffer.contents b
    else
      let () = Buffer.add_char b o.[i] in
      let () = Buffer.add_char b e.[i] in
      aux (succ i)
  in
  aux 0


let () =
  match leno - lene with
  | 0 -> solve leno |> print_endline
  | _ -> solve lene |> fun s -> Printf.printf "%s%c\n" s o.[lene]

