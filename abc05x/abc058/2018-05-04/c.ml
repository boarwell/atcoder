let rep_char c n =
  let b = Buffer.create n in
  for i = 1 to n do Buffer.add_char b c done ;
  Buffer.contents b

let count s c =
  let len = String.length s in
  let rec aux i acc =
    if i >= len then acc
    else
      match String.index_from s i c with
      | j -> aux (succ j) (succ acc)
      | exception _ -> acc
  in
  aux 0 0

let solve ls =
  let rec aux c tmp =
    if c = '{' then List.fast_sort String.compare tmp
    else
      let next = (Char.code c |> succ |> Char.chr) in
      match List.fold_left
              (fun acc s -> min acc (count s c)) 50 ls with
      | 0 -> aux next tmp
      | n -> aux next (rep_char c n :: tmp)
  in
  aux 'a' []

let rec pr_ls = function
  | [] -> print_newline ()
  | h :: t-> print_string h; pr_ls t

let mk_ls n =
  let rec aux n ls =
    if n <= 0 then ls else aux (pred n) (read_line () :: ls)
  in
  aux n []

let () =
  mk_ls (read_int ()) |> solve |> pr_ls