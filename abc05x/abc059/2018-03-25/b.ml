let e = "EQUAL"

and g = "GREATER"

and l = "LESS"

let f s1 s2 =
  let rec aux i =
    if s1 = s2 then e
    else if s1.[i] > s2.[i] then g
    else if s1.[i] < s2.[i] then l
    else aux (i + i)
  in
  aux 0


let () =
  let a = read_line () and b = read_line () in
  ( if String.length a > String.length b then g
    else if String.length a < String.length b then l
    else if a = b then e
    else f a b )
  |> print_endline

