let id x = x

let n = Scanf.scanf "%d\n" id

let l = Array.init n (fun _ -> Scanf.scanf "%d " id) |> Array.to_list

let plus = ref 0  and minus = ref 0

let () =
  let solve sign c =
    List.fold_left
      (fun sum el ->
         let new_sum = sum + el in
         if sum < 0 then
           (if new_sum > 0 then new_sum
            else let () = c := !c + abs new_sum + 1 in 1)
         else
           (if new_sum < 0 then new_sum
            else let () = c := !c + new_sum + 1 in -1)
      )
      ((List.hd l) * sign) (List.tl l)
  in
  let _ = solve 1 plus and _ = solve (-1) minus in
  Printf.printf "%d\n" (min !plus !minus)

