n = gets.to_i
a = gets.split.map(&:to_i)

count = 0
fst = a.shift

ans = a.inject(fst) do |sum, el|
  new_sum = sum + el
  if sum < 0
    if new_sum > 0
      new_sum
    else
      count += new_sum.abs + 1
      1
    end
  else
    if new_sum < 0
      new_sum
    else
      count += new_sum + 1
      -1
    end
  end
end

puts count