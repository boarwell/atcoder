open Char
let () =
  Scanf.scanf "%s %s %s" (fun s1 s2 s3 ->
      Printf.printf "%c%c%c\n" 
        (uppercase s1.[0]) (uppercase s2.[0]) (uppercase s3.[0]))