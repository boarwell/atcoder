let id x = x
let n = Scanf.scanf "%d\n" id
let ar = Array.init n (fun _ -> Scanf.scanf "%d " id)

let rec solve i sum ans =
  if i >= n then ans
  else
    let pre = sum and sum' = sum + ar.(i) in
    if pre * sum' < 0 then solve (succ i) sum' ans
    else solve (succ i) (if pre < 0 then 1 else -1) (ans + (abs sum') + 1)


let () =
  (match ar.(0) with
   | 0 -> min (solve 1 1 1) (solve 1 (-1) 1)
   | a -> min (solve 1 a 0) (solve 1 (if a < 0 then 1 else -1) (abs a + 1)))
  |> Printf.printf "%d\n"

