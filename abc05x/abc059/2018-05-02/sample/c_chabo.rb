# https://beta.atcoder.jp/contests/abc059/submissions/2124441

def solve(n, a, s)
  res=0
  sum=a[0]
  if sum*s <= 0
    res += sum.abs + 1
    sum = s
  end
  1.upto(n-1) do |i|
    pre = sum
    sum += a[i]
    next if pre*sum < 0
    res += sum.abs + 1
    sum = pre<0 ? 1 : -1
  end
  res
end

n=gets.to_i
a=gets.split.map &:to_i
p [solve(n, a.dup, 1), solve(n, a.dup, -1)].min
