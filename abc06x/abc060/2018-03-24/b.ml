let () =
  let a, b, c = Scanf.scanf "%d %d %d" (fun a b c -> (a, b, c)) in
  let rec f tmp =
    let x = tmp mod b in
    match x with 0 -> "NO" | y when y = c -> "YES" | _ -> f (tmp + a)
  in
  f a |> print_endline

