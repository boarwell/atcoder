open String
let () = 
  Scanf.scanf "%s %s %s" (fun a b c ->
      let lena = length a and lenb = length b in
      if a.[lena-1] = b.[0] && b.[lenb-1] = c.[0] then "YES"
      else "NO")
  |> print_endline
