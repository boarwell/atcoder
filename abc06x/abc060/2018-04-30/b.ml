let solve a b c =
  let rec aux i =
    let m = (a * i) mod b in
    match m, c with
    | 0, 0 -> "YES"
    | 0, _ -> "NO"
    | m, c when m = c -> "YES"
    | _ -> aux (succ i) 
  in
  aux 1

let () = Scanf.scanf "%d %d %d" solve |> print_endline