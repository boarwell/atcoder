let solve n t =
  let ar = Array.init n (fun _ ->
      Scanf.scanf "%d " (fun a -> a)) in
  let rec aux i sum =
    if i >= n then sum + t
    else
      let pre = ar.(i-1) and now = ar.(i) in
      let dur = min (now - pre) t in
      aux (succ i) (sum + dur)
  in
  aux 1 0

let () = Scanf.scanf "%d %d\n" solve |> Printf.printf "%d\n"