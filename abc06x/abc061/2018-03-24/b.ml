let scan_words () = Scanf.scanf "%d %d\n" (fun x y -> (x, y))

let () =
  let n, m = scan_words () in
  let ar = Array.make n 0 in
  let rec f i =
    if i = m then ()
    else
      let a, b = scan_words () in
      ar.(a - 1) <- ar.(a - 1) + 1 ;
      ar.(b - 1) <- ar.(b - 1) + 1 ;
      f (i + 1)
  in
  f 0 ;
  Array.iter (Printf.printf "%d\n") ar

