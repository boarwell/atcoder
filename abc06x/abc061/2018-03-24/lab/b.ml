(*
  forを使って書き直したもの
  単純にある回数何かを実行、という処理なら再帰にこだわらずループでいいかも
*)
let scan_words () = Scanf.scanf "%d %d\n" (fun x y -> (x, y))

let () =
  let n, m = scan_words () in
  let ar = Array.make n 0 in
  for i = 1 to m do
    let a, b = scan_words () in 
    ar.(a - 1) <- succ ar.(a - 1);
    ar.(b - 1) <- succ ar.(b - 1);
  done;
  Array.iter (Printf.printf "%d\n") ar