module Im = Map.Make(struct type t = int let compare = compare end)

let f n m =
  let rec aux m' map =
    if m' <= 0 then map
    else
      let a, b = Scanf.scanf "%d %d\n" (fun a b -> a, b) in
      let va = try Im.find a map with _ -> 0 in
      let vb = try Im.find b map with _ -> 0 in
      aux (pred m') (Im.add a (succ va) map |> Im.add b (succ vb))
  in
  let rec pr n' map =
    if n' > n then ()
    else
      (Printf.printf "%d\n" (try Im.find n' map with _ -> 0) ;
       pr (succ n') map)
  in
  aux m Im.empty |> pr 1

let () = Scanf.scanf "%d %d\n" f