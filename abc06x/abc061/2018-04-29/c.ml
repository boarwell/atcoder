let ar = Array.make 100001 0
let scan_words () = Scanf.scanf "%d %d\n" (fun a b -> (a, b))
let n, k = scan_words ()

let rec init_ar n =
  if n <= 0 then ()
  else
    let a, b = scan_words () in
    ar.(a) <- ar.(a) + b ;
    init_ar (pred n)

let rec solve i k =
  let k' = k - ar.(i) in
  if k' <= 0 then i else solve (succ i) k'

let () =
  init_ar n ;
  solve 1 k |> Printf.printf "%d\n"
