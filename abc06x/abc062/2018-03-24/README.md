# メモ

2018-03-24 4:09

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAxSTBzX8mwkYSFQTA3CNB3a/ARC074?dl=0

## A - Grouping

- 愚直に場合分けするしかない……？

4:21

## B - Picture Frame

### [OCaml] Scanfしてからread_*するとうまくいかない？

たぶんそんな気がしてきました。要検証、要注意。


---

4:59

## C - Chocolate Bar

- 方針すら立てられず解説を見ました
- が、OCamlでは実装できませんでした
- 疲れたのでこの問題はまた今度にします
- ほかの方々の実装はRubyのやつがわかりやすそうでした