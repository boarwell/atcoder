# メモ

2018-03-23 23:41

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAB1GaUxb3tNbKrYy68Shl2Va/ARC075?dl=0


## A - Restricted

- 単純な足し算と条件分岐

## B - Varied

- 文字列の頭からインデックスを移動していく
  - それ以降に現在のインデックスが示す文字が含まれているか、を判定

- OCamlにそういう関数が含まれているのが意外だった

23:44

## C - Bugged

- 今回もたくさんWAを出しましたが、前回よりは気持ちが楽です
  - 「10歳でもわかる問題解決の授業」という本で、「早めに失敗して方向性を修正していくのが大事」という考え方が紹介されていてそれに救われました
  - それでも「こんなにミスるなんて情けない、悔しい……」という気持ちが捨てきれません

- 最初にRubyで実装したのは以下のような手順
  - 入力の個数からn個の組み合わせを作って和を求める
  - その和が条件を満たしていたらtmpと比較し、tmpより大きければtmpを更新する
  - n個の組み合わせを試してみてtmpが0だったら(n-1)個の組み合わせを試す

- が、これはTLEを出してしまう

- 次にGoで考えたのは、
  - 最初に和を求める
  - 条件を満たさない場合は、入力の配列の中から最小値を和から引く
  - それでも条件を満たさない場合は、次の最小値を和から引く、と繰り返す
  - 条件に合致した時点で出力

- これだと、和から引きすぎてしまう
  - 引けばいいのは10の倍数ではない数字のうち最小の値のみ
  - この、「10の倍数ではない数字のうち最小の値」を求めるところでミスを連発した
    - その条件に合う数字が配列になかったときは、ゼロ値の0を返してしまっていた
    - その0を引いてそれを出力していたが、それでは条件を満たせない

## 振り返り

- だんだんOCamlにも慣れてきた

- うまくいかないときの原因を探すのが上達した気がする
  - ありうる入力のパターンを考えるのが大事そう

## 終了

2018-03-24 0:39

### 3言語での実装

2018-03-24 1:16