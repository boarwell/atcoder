package main

import "fmt"

var (
	a, b int
)

func main() {
	fmt.Scan(&a, &b)
	if a+b >= 10 {
		fmt.Println("error")
	} else {
		fmt.Println(a + b)
	}
}
