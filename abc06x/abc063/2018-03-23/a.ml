let () =
  Scanf.scanf "%d %d" (fun a b ->
      if a + b >= 10 then "error" else (string_of_int (a + b)))
  |> print_endline