let () = 
  let s = read_line () in
  let rec f i = 
    if i = String.length s then "yes"
    else if String.contains_from s (i + 1) s.[i] then "no"
    else f (i + 1)
  in 
  f 0 |> print_endline