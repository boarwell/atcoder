# https://beta.atcoder.jp/contests/abc063/submissions/1722812
s = gets.chomp
puts s.size == s.chars.uniq.size ? "yes" : "no"
