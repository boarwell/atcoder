let f a b =
  let s = a + b in
  if s >= 10 then print_endline "error"
  else Printf.printf "%d\n" s

let () = Scanf.scanf "%d %d" f
