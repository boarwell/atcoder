let () =
  let s = read_line () in
  let len = String.length s in
  let ar = Array.init len (fun i -> s.[i]) in
  let ls = List.sort_uniq Char.compare (Array.to_list ar) in
  (if len = List.length ls then "yes" else "no") |> print_endline

