module Cs = Set.Make(struct type t = char let compare = Char.compare end)

let s = read_line ()
let len = String.length s

let rec mk_set i se =
  if i >= len then se else mk_set (succ i) (Cs.add s.[i] se)

let () =
  (if len = Cs.cardinal (mk_set 0 Cs.empty) then "yes" else "no")
  |> print_endline