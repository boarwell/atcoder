let n = read_int ()
let ar = Array.init n (fun _ -> read_int ())
let () = Array.fast_sort compare ar

let sum  = 
  let rec f i tmp =
    if i >= n then tmp else f (succ i) (ar.(i) + tmp)
  in
  f 0 0 

let rec solve i =
  if i >= n then 0
  else
    let sum' = sum - ar.(i) in
    if sum' mod 10 = 0 then solve (succ i) else sum'

let () =
  (if sum mod 10 = 0 then solve 0 else sum)
  |> Printf.printf "%d\n"