let () =
  Scanf.scanf "%d %d %d" (fun r g b ->
      (if (r * 100 + g * 10 + b) mod 4 = 0 then "YES" else "NO")
      |> print_endline )

