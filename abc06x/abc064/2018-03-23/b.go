package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

var (
	n   int
	sum int
)

func main() {
	fmt.Scan(&n)
	a := make([]int, 0, n)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	for sc.Scan() {
		a = append(a, intOfString(sc.Text()))
	}

	sort.Ints(a)

	for i := 1; i < n; i++ {
		sum += a[i] - a[i-1]
	}

	fmt.Println(sum)
}

func intOfString(s string) int {
	n, _ := strconv.Atoi(s)
	return n
}
