let () =
  let n = read_int () in 
  let a = Array.init n (fun _ -> Scanf.scanf "%d " (fun x -> x)) in 
  let () = Array.fast_sort compare a in 
  let rec f i sum =
    if i = n then sum else f (i + 1) (sum + a.(i) - a.(i - 1))
  in f 1 0
     |> Printf.printf "%d\n"