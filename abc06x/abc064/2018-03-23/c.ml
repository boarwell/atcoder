let n = read_int ()

let flag = Array.make 8 false

let count_true a =
  let rec f i c =
    if i = Array.length a then c
    else if a.(i) then f (i + 1) (c + 1)
    else f (i + 1) c
  in
  f 0 0


let over = ref 0

let init_flag () =
  let rec f i =
    if i = n then ()
    else
      let () =
        Scanf.scanf "%d " (fun a ->
            if a >= 3200 then over := !over + 1 else flag.(a / 400) <- true )
      in
      f (i + 1)
  in
  f 0


let () =
  init_flag () ;
  let a, b =
    if !over = 0 then (count_true flag, count_true flag)
    else (max (count_true flag) 1, count_true flag + !over)
  in
  Printf.printf "%d %d\n" a b

