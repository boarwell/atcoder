// https://beta.atcoder.jp/contests/abc064/submissions/1595349

package main

import (
	"fmt"
)

func main() {
	var n int
	fmt.Scan(&n)
	other := 0
	flags := make([]bool, 8)
	for i := 0; i < n; i++ {
		var t int
		fmt.Scan(&t)
		if t >= 3200 {
			other++
		} else {
			flags[t/400] = true
		}
	}
	min := 0
	for i := 0; i < 8; i++ {
		if flags[i] {
			min++
		}
	}
	fmt.Println(max(1, min), min+other)
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
