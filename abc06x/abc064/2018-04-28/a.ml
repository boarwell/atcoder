let () =
  Scanf.scanf "%d %d %d" (fun r g b ->
      match (r * 100 + g * 10 + b) mod 4 with
      | 0 -> "YES"
      | _ -> "NO")
  |> print_endline