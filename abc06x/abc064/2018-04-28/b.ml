let id x = x

let mk_ls n =
  let rec f n' ls =
    if n' <= 0 then List.sort_uniq compare ls
    else
      f (pred n') (Scanf.scanf "%d " id :: ls)
  in
  f n []

let solve ls =
  let rec aux ans = function
    | [] | [_] -> ans
    | h :: (h' :: _ as t) ->
      aux (ans + h' - h) t
  in
  aux 0 ls

let () =
  mk_ls (Scanf.scanf "%d\n" id) |> solve |> Printf.printf "%d\n"
