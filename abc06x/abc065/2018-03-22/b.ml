let n = read_int ()

let a = Array.init n (fun _ -> Scanf.scanf "%d\n" (fun x -> x))

let () =
  let rec f count value i =
    if i = n then -1
    else
      let next = a.(value) in
      if next = 2 then count + 1
      else f (count + 1) (next - 1) (i + 1)
  in
  f 0 0 0 |> Printf.printf "%d\n"

