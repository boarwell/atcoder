let () =
  let i = 1000000007 in
  let fact a =
    let rec fact_aux x tmp =
      if x = 1 then tmp mod i else fact_aux (x - 1) (tmp * x mod i)
    in
    fact_aux a 1
  in
  let f n m = fact n mod i * fact m mod i in
  let dogs, monkeys = Scanf.scanf "%d %d" (fun x y -> (x, y)) in
  ( match abs (dogs - monkeys) with
    | 0 -> f dogs monkeys * 2 mod i
    | 1 -> f dogs monkeys mod i
    | _ -> 0 )
  |> Printf.printf "%d\n"

