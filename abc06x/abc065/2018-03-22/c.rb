dogs, monkeys = gets.split.map(&:to_i)
mod = 1e9 + 7
tmp = 1

case (dogs - monkeys).abs
when 0
  1.upto(dogs) do |x|
    tmp = (x * tmp) % mod
  end

  1.upto(monkeys) do |x|
    tmp = (x * tmp) % mod
  end

  puts(((2 * tmp) % mod).floor)

when 1
  1.upto(dogs) do |x|
    tmp = (x * tmp) % mod
  end

  1.upto(monkeys) do |x|
    tmp = (x * tmp) % mod
  end

  puts tmp.floor
else
  puts 0
end
