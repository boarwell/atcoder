package main

import (
	"fmt"
	"os"
)

var (
	dogs, monkies uint64
)

func main() {
	fmt.Scan(&dogs, &monkies)
	if abs(int(dogs)-int(monkies)) != 1 && dogs != monkies {
		fmt.Println(0)
		os.Exit(0)
	}

	if dogs == monkies {
		fmt.Println(
			(fact(1, 1, dogs) * fact(1, 1, monkies) * 2) % 1e7,
		)
	} else {
		fmt.Println(
			(fact(1, 1, dogs) * fact(1, 1, monkies)) % 1e7,
		)
	}

}

func abs(x int) int {
	if x < 0 {
		return -x
	}

	return x
}

func fact(tmp, i, x uint64) uint64 {
	if i > x {
		return tmp
	}

	return fact((tmp*i)%1e7, i+1, x)
}
