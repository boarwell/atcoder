// Try AtCoder
// author: Leonardone @ NEETSDKASU
package main

import "fmt"

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

const MOD = 1e9 + 7

func main() {
	var n, m int
	fmt.Scan(&n, &m)
	switch abs(n - m) {
	case 0:
		tmp := 1
		for i := 1; i <= n; i++ {
			tmp = (tmp * i) % MOD
		}
		for i := 1; i <= m; i++ {
			tmp = (tmp * i) % MOD
		}
		tmp = (2 * tmp) % MOD
		fmt.Println(tmp)
	case 1:
		tmp := 1
		for i := 1; i <= n; i++ {
			tmp = (tmp * i) % MOD
		}
		for i := 1; i <= m; i++ {
			tmp = (tmp * i) % MOD
		}
		fmt.Println(tmp)
	default:
		fmt.Println(0)
	}

}
