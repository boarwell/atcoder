let f x a b =
  if b <= a then "delicious"
  else if b <= a + x then "safe"
  else "dangerous"

let () = Scanf.scanf "%d %d %d" f |> print_endline
