module Int = struct
  type t = int
  let compare = compare
end
module Ints = Set.Make(Int)

let id x = x
let n = Scanf.scanf "%d\n" id
let ar = Array.init n (fun _ -> Scanf.scanf "%d " id)

let rec f i ans s =
  let a = ar.(i-1) in
  if Ints.mem a s then -1
  else if a = 2 then succ ans
  else f a (succ ans) (Ints.add a s)

let () =
  f 1 0 Ints.empty |> Printf.printf "%d\n"