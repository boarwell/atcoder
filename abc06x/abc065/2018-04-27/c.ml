let a = 1000000007

let fact n =
  let rec aux m acc =
    if m = 1 then acc else aux (pred m) (acc * m mod a)
  in
  aux n 1


let solve n m =
  match abs (n - m) with
  | 0 ->
    let l = fact n in
    l * l * 2 mod a
  | 1 -> fact n * fact m mod a
  | _ -> 0


let () = Scanf.scanf "%d %d" solve |> Printf.printf "%d\n"
