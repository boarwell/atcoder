package main

import (
	"fmt"
	"os"
)

var (
	s string
)

func main() {
	fmt.Scan(&s)
	for i := len(s) - 1; i > 0; i-- {
		if yes(s[0:i]) {
			fmt.Println(i)
			os.Exit(0)
		}
	}
}

func yes(s string) bool {
	if len(s)%2 == 1 {
		return false
	}

	if s[0:len(s)/2] == s[len(s)/2:] {
		return true
	}

	return false
}
