let yes s i =
  if i mod 2 = 1 then false 
  else
  if String.sub s 0 (i / 2) = String.sub s (i / 2) (i / 2)
  then true else false

let () =
  let rec f s i = 
    if yes (String.sub s 0 i) i then i
    else f s (i - 1)
  in
  let str = read_line () in
  (f str ((String.length str) - 1)) |> Printf.printf "%d\n"