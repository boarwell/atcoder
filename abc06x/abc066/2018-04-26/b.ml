open String

let is s =
  let len = length s in
  if len mod 2 = 0 then
    let half = len / 2 in
    sub s 0 half = sub s half half
  else false


let rec solve s =
  let len = length s in
  if is s then len else solve (sub s 0 (pred len))


let () =
  let s = read_line () in
  let len = pred @@ length s in
  solve (sub s 0 len) |> Printf.printf "%d\n"

