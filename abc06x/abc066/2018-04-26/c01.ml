(* これだと長い入力でTLEします *)
(* このくらい短く書けるとうれしいのですが *)
let id x = x
let n = Scanf.scanf "%d\n" id
let ar = Array.init n (fun _ -> Scanf.scanf "%s " id)

let rec f i ans =
  if i >= n then ans
  else
    let a = ar.(i) in
    if (i + 1) mod 2 = n mod 2 then f (succ i) (a ^ " " ^ ans)
    else f (succ i) (ans ^ " " ^ a)

let () = f 1 ar.(0) |> print_endline
