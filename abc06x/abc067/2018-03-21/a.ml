let () =
  let a, b = Scanf.scanf "%d %d" (fun x y -> (x, y)) in
  ( if a mod 3 = 0 || b mod 3 = 0 || (a + b) mod 3 = 0 then "Possible"
    else "Impossible" )
  |> Printf.printf "%s\n"

