package main

import (
	"fmt"
	"sort"
)

var (
	n, k int
)

func main() {
	fmt.Scan(&n, &k)
	ls := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&ls[i])
	}
	sort.Sort(sort.Reverse(sort.IntSlice(ls)))

	fmt.Println(sum(0, ls[0:k]))
}

func sum(x int, ls []int) int {
	if len(ls) == 0 {
		return x
	}

	return sum(x+ls[0], ls[1:])
}
