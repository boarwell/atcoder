package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

var (
	n     int
	total int
	sumx  int
	tmp   = math.MaxInt64
)

func main() {
	fmt.Scan(&n)
	a := make([]int, 0, n)
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	for sc.Scan() {
		tmp, _ := strconv.Atoi(sc.Text())
		total += tmp
		a = append(a, tmp)
	}

	for i := 0; i < (n - 1); i++ {
		sumx += a[i]
		tmp = min(tmp, abs(2*sumx-total))
	}
	fmt.Println(tmp)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}
