let f n =
  let rec mkls n' ls sum =
    if n' <= 0 then ls, sum
    else
      let a = (Scanf.scanf "%d " (fun x -> x)) in
      mkls (pred n') (a :: ls) (sum + a)
  in
  let ls, sum = mkls n [] 0 in
  let rec solve tmp ans = function
    | [] | [_] -> ans (* 必ず一枚は取らなければいけないので *)
    | h :: t ->
      let tmp' = h + tmp in
      let ans' = (sum - tmp') - tmp' |> abs in
      solve tmp' (min ans ans') t
  in
  solve 0 max_int ls

let () = Scanf.scanf "%d\n" f |> Printf.printf "%d\n"