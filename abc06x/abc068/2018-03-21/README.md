# メモ

## テストケース

[ABC068/ARC079](https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAf0UF5DqHpSmaLCtKgtIdda/ARC079?dl=0)

## B

http://d.hatena.ne.jp/Kappuccino/20080806/1217990537

## C - Cat Snuke and a Voyage

### fmt.Scanは10^5くらいまで

- 最初に書いた`fmt.Scan`を使うコードがめちゃ遅い
  - だいたい同じアルゴリズムで書いたのにRubyよりも遅い

```sh

$ time go run c.go < last0.txt
2018-03-21 05:29:13.812266858 +0900 JST m=+0.000285443 # 処理の開始
2018-03-21 05:29:19.574635313 +0900 JST m=+5.762653898 # 読み込み終わり
POSSIBLE

real    0m6.106s
user    0m3.407s
sys     0m2.757s

```

- 読み込みだけで6秒近くかかってしまっている

- `bufio.Scanner`に書き換えると

```sh

$ time go run c.go < last0.txt
2018-03-21 05:35:31.450901431 +0900 JST m=+0.000336845
2018-03-21 05:35:31.670431652 +0900 JST m=+0.219867066
POSSIBLE

real    0m0.593s
user    0m0.563s
sys     0m0.113s

```

- というわけで、ぜんぜん違いますので気をつけましょう

- [こちら](https://qiita.com/tnoda_/items/b503a72eac82862d30c6#fmtscan-で軽快に読み込む)の記事によると`10^5`くらいまでが1secで読み込める限界らしいです