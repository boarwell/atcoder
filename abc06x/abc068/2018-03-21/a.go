package main

import "fmt"

var (
	n int
)

func main() {
	fmt.Scan(&n)
	fmt.Printf("ABC%d\n", n)
}
