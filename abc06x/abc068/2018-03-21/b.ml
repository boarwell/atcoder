let () =
  Scanf.scanf "%f" (fun n -> 2. ** floor ((log n) /. (log 2.)) )
  |> int_of_float |> Printf.printf "%d\n"