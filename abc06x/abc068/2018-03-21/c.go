package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var (
	n, m int
	a, b int
	mp   = make(map[int][]int)
	sc   = bufio.NewScanner(os.Stdin)
)

func main() {
	// fmt.Println(time.Now())
	sc.Scan()
	l := strings.Split(sc.Text(), " ")
	n, _ = strconv.Atoi(l[0])
	m, _ = strconv.Atoi(l[1])

	for sc.Scan() {
		l = strings.Split(sc.Text(), " ")
		a, _ = strconv.Atoi(l[0])
		b, _ = strconv.Atoi(l[1])
		mp[a] = append(mp[a], b)
	}

	// fmt.Println(time.Now())
	for _, v := range mp[1] {
		if contain(n, mp[v]) {
			fmt.Println("POSSIBLE")
			os.Exit(0)
		}
	}

	fmt.Println("IMPOSSIBLE")
}

func contain(x int, ls []int) bool {
	if len(ls) == 0 {
		return false
	}

	if ls[0] == x {
		return true
	}

	return contain(x, ls[1:])
}
