let n, m = Scanf.scanf "%d %d\n" (fun n m -> (n, m))

let hash = Hashtbl.create m

let rec init_hash m =
  if m = 0 then ()
  else
    let a, b = Scanf.scanf "%d %d\n" (fun a b -> (a, b)) in
    let ls = b :: (try Hashtbl.find hash a with Not_found -> []) in
    Hashtbl.replace hash a ls ;
    init_hash (m - 1)


let rec iter_search = function
  | [] -> "IMPOSSIBLE"
  | from_first :: rest ->
    let to_n = (try Hashtbl.find hash from_first with Not_found -> []) in 
    if List.exists (( = ) n)  to_n then "POSSIBLE"
    else iter_search rest


let () =
  let () = init_hash m in
  Hashtbl.find hash 1 |> iter_search |> Printf.printf "%s\n"

