n, m = gets.split.map(&:to_i)
h = Hash.new { [] }

m.times do
  a, b = gets.split.map(&:to_i)
  h[a] = h[a].push b
end

h[1].each do |island|
  if h[island].include? n
    puts 'POSSIBLE'
    exit
  end
end

puts 'IMPOSSIBLE'
