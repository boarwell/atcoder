package main

import (
	"fmt"
	"os"
	"time"
)

var (
	n, m int
	a, b int
	mp   = make(map[int][]int)
)

func main() {
	fmt.Println(time.Now())

	fmt.Scan(&n, &m)
	for i := 0; i < m; i++ {
		fmt.Scan(&a, &b)
		mp[a] = append(mp[a], b)
	}

	fmt.Println(time.Now())
	for _, v := range mp[1] {
		if contain(n, mp[v]) {
			fmt.Println("POSSIBLE")
			os.Exit(0)
		}
	}

	fmt.Println("IMPOSSIBLE")
}

func contain(x int, ls []int) bool {
	if len(ls) == 0 {
		return false
	}

	if ls[0] == x {
		return true
	}

	return contain(x, ls[1:])
}
