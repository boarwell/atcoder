let n, m = Scanf.scanf "%d %d\n" (fun a b -> (a,b))

let exclude = function
  | (a, b) -> (if a = 1 || b = n then false else true)

let list = 
  let rec f x ls = 
    if x = 0 then ls
    else 
      let line = (Scanf.scanf "%d %d ") (fun a b -> (a, b)) in 
      if exclude line then f (x - 1) ls else f (x - 1) (line :: ls)
  in f m []


let rec func = function
  | [] -> false
  | (1,snd) :: rest ->
    if List.exists ((=) (snd,n)) list then true else func rest
  | _ :: rest -> func rest



let () =
  (if func list then "POSSIBLE" else "IMPOSSIBLE")
  |>Printf.printf "%s\n"
