(*
  - https://beta.atcoder.jp/contests/abc068/submissions/1471712
  - この方がOCaml界最速最小メモリだった
  - 配列の作り方とか、ループ、読み取り関数など参考になる
*)

let read_ns () = Scanf.scanf "%d %d " (fun x y -> (x, y))

let () =
  let (n, m) = read_ns () in
  let to1 = Array.make (n + 1) false in
  let toN = Array.make (n + 1) false in
  for i = 1 to m do
    let (a, b) = read_ns () in
    if a = 1 then to1.(b) <- true;
    if b = n then toN.(a) <- true;
  done;
  let rec doit i =
    if i = n then "IMPOSSIBLE"
    else if to1.(i) && toN.(i) then "POSSIBLE"
    else doit (i + 1) in
  doit 2 |> print_endline
