# ABC068

2018-04-24

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAf0UF5DqHpSmaLCtKgtIdda/ARC079?dl=0

## A - ABCxxx

- Nに"ABC"をくっつけるだけ

## B - Break Number

- 比較する数値aを用意する
  - nを超えるまでaに2をかける
  - nを超えた時点で`log2 a + 1`

- と思いましたが、これではうまくいきません
  - 具体的にはlogを取るところが不要で単に`n <= a`となった時点で`a-1`を出力すればよいです

- とも思いましたが、これだと2の累乗数が入力されたときに間違います
  - 32 -> 16となります
  - 場合分けはめんどうなので以下のようにします

- nのlog2をfloorする
  - `2^上`をして得られた数が答え


## C - Cat Snuke and a Voyage

- 1から出発できるものとNへ到達できるものをハッシュにまとめる
  - 1から出発できるものの行き先に、Nへ到達できるものがあればPOSSIBLE

- ハッシュでやったのですが、あまり設計がきれいでないように思えます
  - 1から出発できるものはキーに1、値に到達点なのに、Nへ到達できるものはキーに到達点、値にNが入っています
  - find_allするときにキーで引っぱってくる必要があるので仕方がないのですが、美しくない気がします

- 調べてみるとSetも宣言するだけなら（ちゃんとモジュールシステムを理解しなくていいなら）簡単に使えそうです
  - ただし、Setはイミュータブルなデータ構造のようですので、使うなら設計の変更が必要です
    - Setを作るなら、再帰関数の引数に渡して連続的に値を追加する必要がある
    - 毎回同じSetを持ち運ぶ必要がある


## 振り返り

## B

- 方針にけっこう悩んだ
  - こういう問題のときにルートを取るとか、対数を取るとか、という方針を立てること自体は、前にやった問題のおかげでできるようになってきました
  - ですが、正確なところまでつめることはまだ苦手です

## C

- 仕方のないことですが、ハッシュを使うときの設計が「関数型言語っぽく」できないのが気になる
  - グローバル変数的なかたちで、nとhを宣言しなくてはいけないところとか

- これはハッシュの代わりにマップを使うとかすればいいのですが、宣言に行数を使うのでめんどくさがっていつもハッシュを使っています
  - あとはイミュータブルなデータ構造でやるなら、いままでの経験で覚えてきたハッシュを使ったやり方を使えないので、新たに考え直すのがめんどうな気がしています
    - そんなことを言っていたら成長できないのでこれはなんとかしましょう
    - 今度ハッシュを使いたい問題が出てきたらマップでやってみます