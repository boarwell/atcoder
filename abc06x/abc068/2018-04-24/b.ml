let f n = log n /. log 2. |> floor |> ( ** ) 2.

let () = f (read_float ()) |> Printf.printf "%.f\n"