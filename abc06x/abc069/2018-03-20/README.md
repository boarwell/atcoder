# メモ

2018-03-20

- Cまですべて自分で解法を考えられた
  - Cは考慮漏れがあってWAを出してしまったが

- テストケースの場所がわかった
  - [場所はここ](https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAk_SECQ2Nc6SVGii3rHX6Fa?dl=0)
  - ABC, ARC, AGCとわかれているからわからなかったけど、ABCでディレクトリがルート配下にないものは、同日開催のARCの中にある
    - 同日開催のARCはコンテストのトップページからわかる
      - 例: [ABC068のトップページ](https://beta.atcoder.jp/contests/abc068/)に、「同日開催の中級者/上級者向けコンテスト、AtCoder Regular Contest 079はこちらから！」という文言がある

## C - 4-adjacent

- 方向性を自分で考えられた
  - 4で割れる数、2でしか割れない偶数、奇数の3パターン

- 要件を満たすために必要な4で割れる数とその他の数の比を間違えてしまった
  - 本当は`4で割れる数 + 1 >= 奇数、2でしか割れない偶数`だったのに、何を勘違いしたのか`@4 >= (odd / 2) + 1`としてしまっていた

- こういう問題ならよくよく考えていけば答えを出せそうになってきた

### OCamlで入力を受け取る際の注意

#### `read_int ()`と`Scanf.scanf`は同時に使わないほうがよさそう

これらを同時に使っていると、テストケース`1_13.txt`で`exception(int_of_string)`が発生してしまった。

なぜか、ほかの短いケースだと発生しなかった。

原因はよくわからないので、これらを併用するのは避けたほうがよさそうです。