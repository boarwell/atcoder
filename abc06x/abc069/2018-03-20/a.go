package main

import "fmt"

var (
	n, m int
)

func main() {
	fmt.Scan(&n, &m)
	fmt.Println((n - 1) * (m - 1))
}
