package main

import "fmt"

var (
	s string
)

func main() {
	fmt.Scan(&s)
	fmt.Printf("%c%d%c\n", s[0], len(s)-2, s[len(s)-1])
}
