package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var (
	odd, four, two int
)

func main() {
	count()

	if four+1 >= odd {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}

}

func count() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	sc.Scan()

	tmp := 0
	for sc.Scan() {
		tmp, _ = strconv.Atoi(sc.Text())

		if tmp%2 == 1 {
			odd++
		} else if tmp%4 == 0 {
			four++
		} else if tmp%2 == 0 {
			two++
		}
	}

	if two > 0 {
		odd++
	}
}
