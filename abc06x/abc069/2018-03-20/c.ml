let n = Scanf.scanf " %d " (fun x -> x)


let odd, four =
  let rec count two odd four n =
    if n = 0 then (two,odd,four)
    else
      let x = Scanf.scanf " %d " (fun n -> n) in 
      if x mod 2 = 1 then count two (odd+1) four (n-1)
      else if x mod 4 = 0 then count two odd (four+1) (n-1)
      else count (two+1) odd four (n-1)
  in
  let two, odd, four = count 0 0 0 n in 
  if two > 0 then (odd + 1, four) else (odd, four)


let () = 
  print_endline (if (four + 1) >= odd then "Yes" else "No")