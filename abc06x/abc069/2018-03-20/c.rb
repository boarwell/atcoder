gets
two = 0
four = 0
odd = 0

gets.split.map do |x|
  x = x.to_i
  if x.odd?
    odd += 1
  elsif (x % 4).zero?
    four += 1
  else
    two += 1
  end
end

odd += 1 if two > 0

if four + 1 >= odd
  puts 'Yes'
else
  puts 'No'
end
