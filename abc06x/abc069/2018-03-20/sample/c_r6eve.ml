(*TIME: 33ms, MEM: 5120KB*)
let () =
  let n = Scanf.scanf "%d " (fun n -> n) in
  let a = Array.init n (fun _ -> Scanf.scanf "%d " (fun i -> i)) in
  let (e, o, f) =
    Array.fold_left (fun (e, o, f) a ->
        if a mod 4 = 0 then (e, o, f + 1)
        else if a mod 2 = 0 then (e + 1, o, f)
        else (e, o + 1, f))
      (0, 0, 0) a in
  print_endline (if e = 0 && o <= f + 1 || o <= f then "Yes" else "No")
