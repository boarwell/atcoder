package main

import "fmt"

var (
	n int
)

func main() {
	fmt.Scan(&n)

	if n/100 == n%10 {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
