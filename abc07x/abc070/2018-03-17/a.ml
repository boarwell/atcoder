let n = Scanf.scanf "%d" (fun x -> x)

let () =
  print_endline (if n / 100 = n mod 10 then "Yes" else "No")