package main

import "fmt"

var (
	a, b int
	c, d int
)

func main() {
	fmt.Scan(&a, &b, &c, &d)

	fmt.Println(
		plus(
			min(b, d) - max(a, c),
		),
	)

}

func min(x, y int) int {
	if x < y {
		return x
	}

	return y
}

func max(x, y int) int {
	if x < y {
		return y
	}

	return x
}

func plus(x int) int {
	if x < 0 {
		return 0
	}

	return x
}
