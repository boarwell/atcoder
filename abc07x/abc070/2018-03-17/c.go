package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var (
	n   uint64
	tmp uint64
	ans uint64
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	n, _ = strconv.ParseUint(sc.Text(), 10, 64)
	ts := make([]uint64, 0, n)

	for sc.Scan() {
		tmp, _ = strconv.ParseUint(sc.Text(), 10, 64)
		ts = append(ts, tmp)
	}

	ans = lcm(ts[0], ts[1:])
	fmt.Println(ans)
}

func lcm(a uint64, l []uint64) uint64 {
	if len(l) == 0 {
		return a
	}

	b := l[0]
	// n := a * b / gcd(a, b)
	// としてしまうと、a*bのところで桁あふれが起こってしまう
	n := a / gcd(a, b) * b

	return lcm(n, l[1:])
}

func gcd(x, y uint64) uint64 {
	r := x % y

	if r == 0 {
		return y
	}

	return gcd(y, r)
}
