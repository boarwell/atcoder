let rec gcd x y =
  let r = x mod y in 
  if r = 0 then y else gcd y r


let rec lcm a = function
  | [] -> a
  | b :: rest -> 
    let n = a / gcd a b * b in lcm n rest


let rec get_input l n =
  if n = 0 then l
  else get_input (read_int () :: l) (n - 1)


let () =
  read_int ()
  |> get_input []
  |> lcm 1
  |> Printf.printf "%d\n"
