let rec gcd a b =
  match a mod b with
  | 0 -> b
  | r -> gcd b r

let lcm a b = a / (gcd a b) * b


let f n =
  let rec g n' ans =
    if n' = 0 then ans
    else g (pred n') (lcm ans (read_int ()))
  in
  g n 1

let () = f (read_int ()) |> Printf.printf "%d\n"