s = gets.chomp
a = Array.new(26, false)

s.each_char do |c|
  a[c.ord - 97] = true
end

a.each_with_index do |x, i|
  unless x
    puts (i + 97).chr
    exit
  end
end

puts 'None'
