package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var (
	f, s int
	tmp  int
	m    = make(map[int]int)
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	sc.Scan()
	for sc.Scan() {
		tmp, _ := strconv.Atoi(sc.Text())
		m[tmp]++

		if m[tmp] >= 2 {
			if tmp > f {
				s = f
				f = tmp
				m[tmp] -= 2
			} else if tmp > s {
				s = tmp
				m[tmp] -= 2
			}
		}
	}
	fmt.Println(f * s)
}
