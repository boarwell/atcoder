(*https://beta.atcoder.jp/contests/abc071/submissions/1524037*)
let read () = Scanf.scanf "%d " (fun i -> i)

let () =
  let n = read () in
  let a = Array.init n (fun _ -> read ()) in
  Array.fast_sort (fun x y -> y - x) a;
  let rec doit i pre =
    if i = n then 0
    else if a.(i-1) = a.(i) then begin
      if pre <> (-1) then pre * a.(i)
      else doit (i + 2) a.(i)
    end else doit (i + 1) pre in
  doit 1 (-1) |> Printf.printf "%d\n"
