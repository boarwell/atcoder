# ABC071

2018-04-23

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AABQ46Rs4MC7JGWIpf9rrAN5a/ARC081?dl=0

## A - Meal Delivery

- (abs (x - a)) (abs (x - b))を比較する

## B - Not Found

- uniqを使いたいのでリストにします
- 一文字ずつ見ていくのはcharのコードでインクリメントしていけばできそう

## C - Make a Rectangle

- 入力を大きさで降順ソートして、2個出てきたらそれをペアにする
- ペアが2つできたらそこで終了


## 振り返り

- この回はどれも簡単に思えた
  - まあ2回目なので当然かもしれませんが
