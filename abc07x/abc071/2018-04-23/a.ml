let () =
  Scanf.scanf "%d %d %d" (fun x a b ->
      if (abs (x - a)) - (abs (x - b)) > 0 then "B" else "A")
  |> print_endline