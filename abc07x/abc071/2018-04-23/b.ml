let mkls s =
  let len = String.length s in
  let rec f i ls =
    if i < 0 then List.sort_uniq Char.compare ls
    else f (i - 1) (s.[i] :: ls) 
  in
  f (len - 1) []

let solve ls = 
  let i2s c = Char.chr c |> Char.escaped in
  if List.length ls = 26 then "None"
  else
    let rec f c = function
      | [] -> i2s c
      | h :: t when Char.chr c <> h -> i2s c
      | _ :: t -> f (c + 1) t
    in
    f (Char.code 'a') ls

let () = mkls (read_line ()) |> solve |> print_endline