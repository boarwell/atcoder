let mkls n =
  let rec f n' ls =
    if n' = 0 then List.fast_sort (fun i j -> j - i) ls
    else
      f (pred n') (Scanf.scanf "%d " (fun x -> x) :: ls)
  in
  f n []

let solve ls =
  let rec f ed = function
    | [] | [_] -> 0
    | h :: (h' :: t as t') ->
      if h = h' && ed > 0 then h * ed
      else if h = h' && ed <= 0 then f h t
      else f ed t'
  in
  f 0 ls

let () = Scanf.scanf "%d\n" mkls |> solve |> Printf.printf "%d\n"