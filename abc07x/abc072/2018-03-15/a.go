package main

import (
	"fmt"
)

var (
	x, t int
)

func main() {
	fmt.Scan(&x, &t)
	if x-t < 0 {
		fmt.Println(0)
	} else {
		fmt.Println(x - t)
	}
}
