# メモ

2018-03-15

## A - Sandglass2

- Ruby版が実質2行で書けたのがうれしい
  - Rubyにも慣れてきた？
  - 条件演算子を使えた
    - 「可読性低い」みたいに言われるらしいけど、こういうふうに使うなら便利ですよね


## B - OddString

- 文字列のインデックスと、いわゆる「n文字目」がずれることに注意
  - 文字列は`0-indexed`、日本語では`1-indexed`

- Goではバッファを使った
  - 空文字列を用意して`+`結合でもいいかもしれないけど、入力の最大長が`10^5`なのでこちらのほうがベター？

- Rubyでは`StringIO`というのが同様の用途で使えそう
  - https://stackoverflow.com/questions/10323/why-doesnt-ruby-have-a-real-stringbuffer-or-stringio
  - 以下のような感じっぽい

```ruby

s = StringIO.new
s << 'hoge'
s << 'fuga'
s.string #=> hogefuga

```

## C - Together

- 最初に書いたGoでは入力に対してx3のループを行っているのでかなり遅い
  - けどTLEしなかった、よかった

- 入力を受け取るループでマップの値をいじるなどの処理をしてしまえればいい
  - `bufio.Scanner`だとそんな感じに書きやすい

- 今回も自分で解けたのでよかった