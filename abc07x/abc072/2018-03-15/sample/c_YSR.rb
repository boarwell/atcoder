# https://beta.atcoder.jp/contests/abc072/submissions/2167075
gets
table = Hash.new(0)
gets.split.map(&:to_i).map{|p| [p-1,p,p+1]}.flatten.each{|p| table[p] += 1}
puts table.values.max

# この方はaの配列を入力の段階で水増しする作戦のようです。
# 水増し、というか、+1, 0, -1の要素を配列に含めてしまうという感じですね。
# それをArray.flattenで一つの配列にして登場回数を数えて出力。