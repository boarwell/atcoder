# https://beta.atcoder.jp/contests/abc072/submissions/2105749

s=[0]*100002
gets
gets.split.each{|i|s[i.to_i]+=1}
p (s.size-1).times.map{|i|s[i-1]+s[i]+s[i+1]}.max

# aの配列を作っておいて、インデックスとaを対応付ける。
# 値には登場回数を保存しておいて、取り出すときに前後の値も同時に取り出すことで仕様を満たしているっぽい。
# この方のコードはいつも短くてすごい
# これは実行時間も69msになっていて、自分がGoで書いたコードよりも速い