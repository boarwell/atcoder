# ABC072

2018-04-21

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAA3R4HO5wnesEdRr8RiIEDja/ARC082?dl=0

## A - Sandglass2

- 引き算するだけ

## B - OddString

- Rubyならcharsでバラしてeach_with_index

- OCamlならstringをArrayにしてiteriでバッファに書き込む

## C - Together

- 読み取ったa, a-1, a+1をそれぞれハッシュに追加していけば解ける
  - が、効率を考えるなら、記録するときは手を加えず、取り出すときにa-1,a,a+1の和を求めて、それの和を出すといい

- 前回も非効率なコードを書いていたようですが、まったく覚えていませんでした