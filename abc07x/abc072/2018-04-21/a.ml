let () =
  Scanf.scanf "%d %d" (fun x t -> max 0 (x - t))
  |> Printf.printf "%d\n"