package main

import (
	"fmt"
	"strings"
)

func main() {
	var s string
	fmt.Scan(&s)
	if strings.Contains(s, "9") {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
