# ABC073

2018-04-21

## テストケース

不明

## A - September 9

- n mod 10 = 9 || n / 10 = 9

## B - Theater

- 素直に入力を足していけばよさそう
  - r - l + 1

## C - Write and Erase

- ハッシュテーブルでサクッと書けそう
  - 簡単そうなので練習のためにマップを使ってみます

- そんなことしなくても数を数えればよくないですか？
  - 最大長の配列にtrue/falseで値を入れておく
  - マップとどっちが速いのか検証するためにも両方実装してみます

- 10^9の長さの配列は作れなかったのでハッシュテーブルだけでやりました
  - マップはInt64ではキーの扱いがうまくいかなかった（よくわからないけどエラー）のでハッシュテーブルです
  - OCamlよりGoのほうが速かった