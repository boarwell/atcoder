let () =
  ( match read_int () with
  | n when n mod 10 = 9 || n / 10 = 9 -> "Yes"
  | _ -> "No" )
  |> print_endline

