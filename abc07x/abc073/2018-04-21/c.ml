open Hashtbl

let h = create 1000

let f n =
  let rec g n' =
    if n' <= 0 then ()
    else
      let a = read_int () in
      (if mem h a then replace h a (not (find h a))
       else add h a true);
      g (n' - 1)
  in
  g n

let () =
  f (read_int ()) ;
  fold (fun _ v s -> if v then s + 1 else s) h 0
  |> Printf.printf "%d\n"