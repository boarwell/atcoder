(*この長さの配列は作れないらしい*)
let ar = Array.make 1000000001 false

let f n =
  let rec g n' =
    if n' <= 0 then ()
    else
      let a = read_int () in
      ar.(a) <- not ar.(a) ;
      g (n' - 1)
  in
  g n

let () =
  f (read_int ());
  Array.fold_left (fun sum b -> if b then sum + 1 else sum) 0 ar
  |> Printf.printf "%d\n"