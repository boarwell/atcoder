package main

import (
	"fmt"
)

var (
	n, a int
)

func main() {
	fmt.Scan(&n, &a)
	fmt.Println(n*n - a)
}
