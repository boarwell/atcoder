let f n k =
  let rec g i sum =
    if i = 0 then sum
    else
      let xi = Scanf.scanf "%d " (fun x -> x) in
      g (i - 1) (sum + min (abs (2 * xi)) (abs (k - xi) * 2))
  in
  g n 0


let () = Scanf.scanf "%d\n%d\n" f |> Printf.printf "%d\n"
