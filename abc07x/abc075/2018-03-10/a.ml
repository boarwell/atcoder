let () =
  Scanf.scanf "%d %d %d" (fun a b c ->
      if a = b then c else if b = c then a else b)
  |> Printf.printf "%d\n";;

let () =
  let a, b, c = Scanf.scanf "%d %d %d" (fun x y z -> (x, y, z)) in
  if a = b then Printf.printf "%d\n" c
  else if b =c then Printf.printf "%d\n" a
  else Printf.printf "%d\n" b;;