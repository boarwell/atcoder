package main

import (
	"bytes"
	"fmt"
	"strconv"
)

var (
	dx = [8]int{1, 0, -1, 0, 1, -1, -1, 1}
	dy = [8]int{0, 1, 0, -1, 1, 1, -1, -1}
)

func main() {
	var h, w int
	fmt.Scan(&h, &w)

	board := make([]string, 0, 50)
	for i := 0; i < h; i++ {
		var line string
		fmt.Scanln(&line)
		board = append(board, line)
	}

	// board[i][j] = numで書き換えられないのでバッファにためておく作戦です
	// Go1.10からはstrings.BUilderも使えるようですがこっちで行きます
	// https://qiita.com/tenntenn/items/94923a0c527d499db5b9
	buf := bytes.NewBufferString("")
	newboard := make([]string, 0, len(board))

	for i := 0; i < h; i++ {
		buf.Reset()

		for j := 0; j < w; j++ {
			if board[i][j] == '#' {
				buf.WriteRune('#')
				continue
			}

			num := 0
			for d := 0; d < 8; d++ {
				ni := i + dy[d]
				nj := j + dx[d]

				if ni < 0 || h <= ni {
					continue
				}
				if nj < 0 || w <= nj {
					continue
				}
				if board[ni][nj] == '#' {
					num++
				}
			}
			buf.WriteString(strconv.Itoa(num))
		}
		newboard = append(newboard, buf.String())
	}

	for _, v := range newboard {
		fmt.Println(v)
	}
}
