let () =
  Scanf.scanf "%d %d %d" (fun a b c ->
      if a = b then c else if b = c then a else b)
  |> Printf.printf "%d\n"