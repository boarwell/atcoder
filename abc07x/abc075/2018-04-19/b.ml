let h, w = Scanf.scanf "%d %d\n" (fun a b -> (a, b))
let ar = Array.init h (fun _ -> Scanf.scanf "%s\n" (fun x -> x))
let b = Buffer.create (h * w)
let d = [(0, 1); (1, 1); (1, 0); (1, -1); (0, -1); (-1, -1); (-1, 0); (-1, 1)]

let search pos =
  List.fold_left
    (fun acc (x, y) ->
       let ni, nj = (fst pos + y, snd pos + x) in
       if match (ni, nj) with
         | a, _ when a < 0 || h <= a -> false
         | _, b when b < 0 || w <= b -> false
         | _, _ -> true
       then if ar.(ni).[nj] = '#' then acc + 1 else acc
       else acc)
    0 d


let () =
  for i = 0 to h - 1 do
    for j = 0 to w - 1 do
      if ar.(i).[j] = '#' then Buffer.add_char b '#'
      else Buffer.add_string b (search (i, j) |> string_of_int)
    done ;
    Buffer.add_string b "\n"
  done ;
  Buffer.contents b |> print_string

