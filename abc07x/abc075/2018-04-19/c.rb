n, m = gets.split.map(&:to_i)
@g = Array.new(n) { [] }
es = []
m.times do
  a, b = gets.split.map(&:to_i)
  @g[a - 1] << b - 1
  @g[b - 1] << a - 1
  es << [a - 1, b - 1]
end

def dfs(v)
  @used[v] = true
  @g[v].each do |u|
    next if @used[u] || @ex == [v, u]
    dfs(u)
  end
end

ans = 0
es.each do |e|
  @ex = e
  @used = Array.new(n, false)
  dfs(e[0])
  ans += 1 unless @used[e[1]]
end

p ans
