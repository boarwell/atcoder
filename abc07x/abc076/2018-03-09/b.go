package main

import "fmt"

func main() {
	var n, k int
	fmt.Scan(&n, &k)

	now := 1
	for i := 0; i < n; i++ {
		if now > k {
			now += k
		} else {
			now *= 2
		}
	}
	fmt.Println(now)
}
