n, k = 2.times.map {gets.to_i}
now = 1

n.times do
  now = now > k ? now + k : now * 2
end

puts now
