let f n k =
  let rec g n' tmp =
    if n' = 0 then tmp
    else if tmp < k then g (n' - 1) (tmp * 2)
    else g (n' - 1) (tmp + k)
  in
  g n 1

let () = Scanf.scanf "%d\n%d" f |> Printf.printf "%d\n"