package main

import (
	"fmt"
	"sort"
)

func main() {
	var n int
	fmt.Scan(&n)

	achan := make(chan struct{})
	bchan := make(chan struct{})
	cchan := make(chan struct{})

	//m := make(map[int]int) // Bi以上のCの個数のマップ

	a := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&a[i])
	}

	go func(a []int) {
		sort.Ints(a)
		close(achan)
	}(a)

	b := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&b[i])
	}
	go func(b []int) {
		sort.Sort(sort.IntSlice(b))
		close(bchan)
	}(b)

	c := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&c[i])
	}
	go func(c []int) {
		sort.Sort(sort.IntSlice(c))
		close(cchan)
	}(c)

	<-achan
	<-bchan
	<-cchan

	count := 0
	for _, v := range b {
		tmp := howManyLower(a, v) * howManyHigher(c, v)
		fmt.Println(tmp)
		count += tmp
	}
	fmt.Println(count)
}

func howManyLower(x []int, n int) int {
	s := len(x)
	if n >= x[s-1] {
		return s
	} else if n < x[0] {
		return 0
	}

	l := 0
	c := s / 2
	r := s - 1

	for l < r {
		if x[c] < n {
			l = c + 1
		} else if x[c] > n {
			r = c
		} else {
			return c
		}
		c = (l + r) / 2
	}

	return l

}

func howManyHigher(x []int, n int) int {
	s := len(x)
	if n >= x[s-1] {
		return 0
	} else if n < x[0] {
		return s
	}

	l := 0
	c := s / 2
	r := s - 1

	for l < r {
		if x[c] < n {
			l = c + 1
		} else if x[c] > n {
			r = c
		} else {
			return s - c - 1
		}
		c = (l + r) / 2
	}

	return s - l

}
