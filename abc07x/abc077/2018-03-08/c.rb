gets
A,B,C=3.times.map{gets.split.map(&:to_i).sort}
 
s=B.map{|b|
  i=C.bsearch_index{|c|b<c}
  i ? C.size-i : 0
}
 
s<<0
(s.size-1).downto(1){|i|s[i-1]+=s[i]}
 
p A.map{|a|
  i=B.bsearch_index{|b|a<b}
  i ? s[i] : 0
}.inject(:+)