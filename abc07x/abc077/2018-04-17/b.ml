let f n = (sqrt n |> floor) ** 2.

let () = f (read_float ()) |> Printf.printf "%.f\n"