let rec smaller n ar =
  let rec f h t =
    let m = (h + t) / 2 in
    if h = m then if ar.(h) < n then h + 1 else h
    else if ar.(m) < n then f m t
    else f h m
  in
  f 0 (Array.length ar)


let rec larger n ar =
  let rec f h t =
    let m = (h + t) / 2 in
    if h = m then
      if ar.(h) <= n then Array.length ar - (h + 1) else Array.length ar - h
    else if ar.(m) <= n then f m t
    else f h m
  in
  f 0 (Array.length ar)


let id x = x
let n = Scanf.scanf "%d\n" id
let a = Array.init n (fun _ -> Scanf.scanf "%d " id)
let b = Array.init n (fun _ -> Scanf.scanf "%d " id)
let c = Array.init n (fun _ -> Scanf.scanf "%d " id)

let () =
  Array.fast_sort ( - ) a ;
  Array.fast_sort ( - ) b ;
  Array.fast_sort ( - ) c ;
  Array.fold_left (fun pre n -> smaller n a * larger n c + pre) 0 b
  |> Printf.printf "%d\n"

