package main

import "fmt"

func main() {
	var x, y string
	fmt.Scan(&x, &y)

	m := map[string]int{
		"A": 10,
		"B": 11,
		"C": 12,
		"D": 13,
		"E": 14,
		"F": 15,
	}

	if m[x] < m[y] {
		fmt.Println("<")
	} else if m[x] > m[y] {
		fmt.Println(">")
	} else {
		fmt.Println("=")
	}

}
