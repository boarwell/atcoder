package main

import (
	"fmt"
	"os"
)

func main() {
	var (
		n, z, w int // fmt.Scan()ではnを無視できないようです
		a       = make([]int, 0, n)
		tmp     int
	)

	fmt.Scanln(&n, &z, &w)

	for i := 0; i < n; i++ {
		// tmpを介さずにScan(&a[i])とやるとindex out of rangeで死にます
		fmt.Scan(&tmp)
		a = append(a, tmp)
	}

	// a[len(a) - 2]でindex out of rangeになってしまうため
	if len(a) < 1 {
		fmt.Println(a[0] - w)
		os.Exit(0)
	}

	fmt.Println(
		// 最後の,についてはlab/break-a-longline.goを参照せよ
		max(
			abs(a[len(a)-1]-w),
			abs(a[len(a)-1]-a[len(a)-2]),
		),
	)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func max(x, y int) int {
	if x >= y {
		return x
	}
	return y
}
