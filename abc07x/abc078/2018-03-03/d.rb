n, z, w = gets.split.map(&:to_i)
# aが1つだけのときに配列ではなく数値として扱われてしまうと困るので
# *を使っています
*a = gets.split.map(&:to_i)

return puts(a[0] - w) if a.size < 2

if (a[-1] - a[-2]).abs > (a[-1] - w).abs
  puts (a[-1] - a[-2]).abs
else
  puts (a[-1] - w).abs
end
