# 実装メモ

2018-03-01

## A

- 「3つ連続した場合」を判定するアルゴリズムに悩んだ。

## B

- 素朴に実装してしまうと最大の入力値である86のときに実行制限時間にひっかかってしまう
- メモ化などの工夫が必要
- グローバルmapを初めて使った（宣言した）
  - [参考:](https://stackoverflow.com/questions/15178088/create-global-map-variables)
  - 内部に状態を持つ型（map, slice, chanなど？）は`make`か`type{}`で初期化する

## C

- めちゃめちゃ時間かかった
- 式を出力する必要があるので、そのための情報をどのように保持するかという設計が難しかった
  - 最終的に毎回文字列を作るという形に
- 再帰のアルゴリズムをうまく作るいい練習になった
  - 標準パターンと再帰パターン
  - 深さ優先探索において、どのように枝を増やしていくか
    - `next(current+nums[pos], pos+1, fmt.Sprintf("%s+%d", s, nums[pos]))`
    - `next(current-nums[pos], pos+1, fmt.Sprintf("%s-%d", s, nums[pos]))`
    - +, - で増やしていく

## D

- 手がつけられなかった
- `karaitarako`さんの[このコード](https://beta.atcoder.jp/contests/abc079/submissions/2145109)を写経した
- `fmt.Scan()`の使い方を覚えた
  - `fmt.Scan()`は長大な入力だとパフォーマンスの問題があるらしいが、これくらいの入力ならサクッと実装できるので便利
  - [参考](https://qiita.com/tnoda_/items/b503a72eac82862d30c6#fmtscan-%E3%81%A7%E8%BB%BD%E5%BF%AB%E3%81%AB%E8%AA%AD%E3%81%BF%E8%BE%BC%E3%82%80)
