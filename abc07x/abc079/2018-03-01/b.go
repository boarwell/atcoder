package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var (
	m = make(map[int]int)
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	in, _ := strconv.Atoi(sc.Text())
	fmt.Println(luca(in))
}

func luca(n int) int {
	x, ok := m[n]
	if ok {
		return x
	}

	if n == 0 {
		m[n] = 2
		return 2
	} else if n == 1 {
		m[n] = 1
		return 1
	}

	m[n] = luca(n-1) + luca(n-2)
	return m[n]
}
