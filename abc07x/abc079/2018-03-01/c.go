package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var (
	// 入力の数値を入れる
	nums = []int{}
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	in := strings.Split(sc.Text(), "")
	nums = ss2Is(in)

	next(0, 0, "")
}

func ss2Is(ss []string) []int {
	is := make([]int, 0, len(ss))
	for _, v := range ss {
		n, _ := strconv.Atoi(v)
		is = append(is, n)
	}

	return is
}

// current: 現在の計算結果
// pos: numのインデックス
// s: 計算式の文字列表現
func next(current int, pos int, s string) {
	if pos == 0 {
		next(nums[0], pos+1, fmt.Sprint(nums[0]))
		return
	} else if pos == 4 {
		if current == 7 {
			fmt.Printf("%s=7\n", s)
			os.Exit(0)
		}
		return
	}

	next(current+nums[pos], pos+1, fmt.Sprintf("%s+%d", s, nums[pos]))
	next(current-nums[pos], pos+1, fmt.Sprintf("%s-%d", s, nums[pos]))
}
