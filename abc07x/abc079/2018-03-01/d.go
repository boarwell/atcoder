package main

import (
	"fmt"
)

var (
	h, w int
	c    = [10][10]int{}
	a    int
	ans  int
)

func main() {
	fmt.Scanln(&h, &w)
	for i := 0; i < 10; i++ {
		for j := 0; j < 10; j++ {
			fmt.Scan(&c[i][j])
		}
	}

	// このループの発想はいままでの自分にはなかった
	for k := 0; k < 10; k++ {
		for i := 0; i < 10; i++ {
			for j := 0; j < 10; j++ {
				// math.Min()を使いたかったが、
				// float64()の壁に阻まれ断念
				// https://mrekucci.blogspot.jp/2015/07/dont-abuse-mathmax-mathmin.html
				// にあるように、変な誤差を生まないためにもintは普通に比較したほうが良さそう
				if c[i][j] > c[i][k]+c[k][j] {
					c[i][j] = c[i][k] + c[k][j]
				}
			}
		}
	}

	for i := 0; i < h; i++ {
		for j := 0; j < w; j++ {
			fmt.Scan(&a)
			if a != -1 {
				ans += c[a][1]
			}
		}
	}

	fmt.Println(ans)
}
