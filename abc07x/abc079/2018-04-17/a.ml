let solve s =
  if s.[0] = s.[1] && s.[1] = s.[2] || s.[1] = s.[2] && s.[2] = s.[3] then
    "Yes"
  else "No"


let () = read_line () |> solve |> print_endline
