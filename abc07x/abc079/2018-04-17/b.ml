let lucas n =
  let rec f i pre now =
    if i = n then now else f (i + 1) now (pre + now)
  in
  if n = 0 then 2 else f 1 2 1

let () = read_int () |> lucas |> Printf.printf "%d\n"
