package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	in := strings.Split(sc.Text(), " ")
	n, _ := strconv.Atoi(in[0])
	a, _ := strconv.Atoi(in[1])
	b, _ := strconv.Atoi(in[2])

	x := math.Min(float64(n*a), float64(b))
	fmt.Println(x)
}
