package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

var (
	ans int
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	n, _ := strconv.Atoi(sc.Text())
	f := make([][]string, 0, 10)
	for i := 0; i < n; i++ {
		sc.Scan()

		f = append(f, strings.Split(sc.Text(), " "))
	}
	p := make([][]int, 0, 11)
	for i := 0; i < n; i++ {
		sc.Scan()
		tmp := strings.Split(sc.Text(), " ")
		line := convSs2Is(tmp)
		p = append(p, line)
	}
	fmt.Println(p)
}

// []stringを[]intに変換します
func convSs2Is(s []string) []int {
	i := make([]int, 0, len(s))
	for _, v := range s {
		x, err := strconv.Atoi(v)
		if err != nil {
			log.Fatalln("failed to strconv in convSs2Is")
		}
		i = append(i, x)
	}
	return i
}

func score(n int, f [][]string, p [][]int, a [10]string) {
	for i, v := range a {

	}
}
