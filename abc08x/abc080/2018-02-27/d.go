package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

// 0. 入力を処理する
// 1. チャンネルの個数分boolスライスを用意する -> timeTable [チャンネル][時刻]bool
// 2. チャンネルiでs-tの間録画する場合は、[i][s]timeTable-[i][t]timeTableをすべてtrueにする
//   録画開始の0.5前から録画機を用意する必要があるため、c1のs, c2のtが重なった場合2つの録画機が必要になるため[i][t]timeTableもtrueにする（問題の条件では「tは含めない」とあるが）
// 3. timeTableを頭から見ていって、時刻iで必要となる録画機をxiとしたとき、xiの最大値を求める

func main() {
	// 0.
	//
	//
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()

	// 番組数はいらなさそう
	channels, _ := strconv.Atoi(strings.Split(sc.Text(), " ")[1])

	// 1.
	//
	//
	// チャンネルは1-indexedなのでlen, capともに+1しています
	timeTable := make([][100001]bool, channels+1, 31)

	var (
		s, t, c int
	)

	for sc.Scan() {
		s, t, c = splitLine(sc.Text())
		// 2.
		//
		//
		addProgram(s, t, c, timeTable)
	}

	// 3.
	//
	//
	// 必要な録画機の台数、今回求めるべきもの（x）
	// math.Max()を使うためにfloat64で宣言します
	var maxNecesssity float64

	// 時刻のループ
	for i := 1; i < 100001; i++ {
		// その時刻でいくつ録画機が必要か、という変数
		tmp := 0
		// チャンネルのループ
		for j := 1; j < channels+1; j++ {
			// programsは[][]boolですが、わかりやすさのためtrueと比較します
			if timeTable[j][i] == true {
				tmp++
			}
		}
		maxNecesssity = math.Max(float64(maxNecesssity), float64(tmp))
	}

	fmt.Println(maxNecesssity)
}

// 2行目以降を受け取ってs, t, cに分解する関数
func splitLine(line string) (s, t, c int) {
	in := strings.Split(line, " ")
	s, _ = strconv.Atoi(in[0])
	t, _ = strconv.Atoi(in[1])
	c, _ = strconv.Atoi(in[2])

	return
}

// チャンネルcのs-tをtrueにする関数
func addProgram(s, t, c int, timeTable [][100001]bool) {
	// tもtrueにするため、i < t+1
	for i := s; i < t+1; i++ {
		timeTable[c][i] = true
	}
}
