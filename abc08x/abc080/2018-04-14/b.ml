let solve n =
  let rec f x sum = if x < 1 then sum else f (x / 10) (x mod 10 + sum) in
  if n mod f n 0 = 0 then "Yes" else "No"

let () = solve (read_int ()) |> print_endline