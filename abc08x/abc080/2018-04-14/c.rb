n = gets.to_i
fs = Array.new(n) { gets.delete(' ').to_i(2) }
ps = Array.new(n) { gets.split.map(&:to_i) }

pmax = -Float::INFINITY

1.upto(1023) do |b|
  x = 0
  n.times do |i|
    x += ps[i][(fs[i] & b).to_s(2).count('1')]
  end
  pmax = pmax < x ? x : pmax
end

p pmax