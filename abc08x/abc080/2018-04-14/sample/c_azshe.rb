# https://beta.atcoder.jp/contests/abc080/submissions/1832240

n = gets.to_i
F = n.times.map{gets.gsub(/\s+/,"").to_i(2)}
P = n.times.map{gets.split.map(&:to_i)}

pmax = -Float::INFINITY

1.upto((1<<10)-1) do |b|
  p = 0
  n.times do |i|
    p += P[i][(F[i] & b).to_s(2).count("1")]
  end
  pmax = [pmax, p].max
end

p pmax
