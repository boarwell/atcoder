let count target s =
  let rec f i tmp =
    if i = String.length s then tmp
    else if s.[i] = target then f (i + 1) (tmp + 1)
    else f (i + 1) tmp
  in
  f 0 0


let () = Scanf.scanf "%s" (fun s -> s) 
         |> count '1' |> Printf.printf "%d\n"
