package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	// 最初の行は必要ない
	sc.Scan()
	in := strings.Split(sc.Text(), " ")
	nums := make([]int, 0, len(in))
	for _, v := range in {
		x, _ := strconv.Atoi(v)
		nums = append(nums, x)
	}

	count := 0
	for _, v := range nums {
		// 奇数があったら即刻KO
		if v%2 != 0 {
			fmt.Println(0)
			os.Exit(0)
		}

		tmp := 0
		for v%2 == 0 {
			v /= 2
			tmp++
		}
		// 初回のループではcount > tmpだけだとうまく更新できない
		if count == 0 && tmp != 0 || count > tmp {
			count = tmp
		}
	}
	fmt.Println(count)

}
