let cnt n =
  let rec f n' c = if n' mod 2 = 1 then c else f (n' / 2) (c + 1) in
  f n 0

let solve n =
  let rec f i ans =
    if i > n then ans
    else let tmp = Scanf.scanf "%d " cnt in f (i + 1) (min ans tmp)
  in
  f 1 max_int


let () = Scanf.scanf "%d\n" solve |> Printf.printf "%d\n"