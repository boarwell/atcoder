open Hashtbl

let id x = x
let n = Scanf.scanf "%d " id
let h = create n
let k = Scanf.scanf "%d\n" id
let ans = ref 0

let find_min () =
  fold
    (fun k v tmp ->
       let k', v' = tmp in
       if v < v' then (k, v) else tmp)
    h (0, max_int)


let inith () =
  let rec f i =
    if i <= n then
      let a = Scanf.scanf "%d " id in
      let v = try find h a with _ -> 0 in
      replace h a (v + 1) ;
      f (i + 1)
  in
  f 1


let () =
  inith () ;
  let len = ref (length h) in
  while k < !len do
    let v = find_min () in
    decr len ;
    remove h (fst v) ;
    ans := !ans + snd v
  done ;
  Printf.printf "%d\n" !ans

