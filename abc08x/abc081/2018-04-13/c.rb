n, k, *a = $stdin.read.split.map(&:to_i)

b = Array.new(n+1, 0)
a.each do |x|
  b[x] += 1
end

b.delete(0)
len = b.length
c = b.sort.reverse
if c.length <= k
  p 0
else
  p c[k, len - k].inject(:+)
end