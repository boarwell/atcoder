let n, k = Scanf.scanf "%d %d\n" (fun n k -> (n, k))

let a = Array.init n (fun _ -> Scanf.scanf "%d " (fun x -> x))

let a' = Array.make n 0

let () =
  Array.iter (fun x -> a'.(x - 1) <- a'.(x - 1) + 1) a ;
  let a'' = List.filter (( <> ) 0) @@ Array.to_list a' in
  let len = List.length a'' and a''' = List.fast_sort (fun i j -> j - i) a'' in
  ( if List.length a''' <= k then 0
  else Array.sub (Array.of_list a''') k (len - k) |> Array.fold_left ( + ) 0 )
  |> Printf.printf "%d\n"

