package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	in := strings.Split(sc.Text(), " ")
	a, _ := strconv.Atoi(in[0])
	b, _ := strconv.Atoi(in[1])

	mean := (float64(a) + float64(b)) / 2
	fmt.Println(math.Round(mean))
}
