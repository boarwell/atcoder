package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	sc.Scan()
	rawin := strings.Split(sc.Text(), " ")
	in := make([]int, 0, len(rawin))
	for _, v := range rawin {
		n, _ := strconv.Atoi(v)
		in = append(in, n)
	}

	// 取り除く個数
	del := 0
	// すでに遭遇した数字かどうか
	m := map[int]bool{}

	for _, v := range in {
		if m[v] {
			continue
		}
		// v == howMany(v, in)なら何もしない
		if v > howMany(v, in) {
			del += howMany(v, in)
		} else if v < howMany(v, in) {
			del += howMany(v, in) - v
		}
		m[v] = true
	}
	fmt.Println(del)
}

// how many x in list?
func howMany(x int, list []int) int {
	count := 0
	for _, v := range list {
		if v == x {
			count++
		}
	}
	return count
}

func remove(x int, list []int) {
	new := make([]int, 0, len(list))
	for _, v := range list {
		if x != v {
			new = append(new, v)
		}
	}
}
