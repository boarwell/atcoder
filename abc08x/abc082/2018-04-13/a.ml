let () =
  Scanf.scanf "%f %f" (fun a b ->
      ((a +. b) /. 2.) +. 0.9) |> floor |> Printf.printf "%.f\n"