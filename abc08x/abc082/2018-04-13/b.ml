let sorts s b =
  let s2l s =
    let len = String.length s in
    let rec f i l =
      if i >= len then
        let ls = List.sort Char.compare l in
        if b then List.rev ls else ls
      else f (i + 1) (s.[i] :: l)
    in
    f 0 []
  in
  let rec cl2s tmp = function
    | [] -> tmp
    | c :: t -> cl2s (Char.escaped c ^ tmp) t
  in
  s2l s |> cl2s ""


let f s t =
  let s' = sorts s true and t' = sorts t false in
  if s' < t' then "Yes" else "No"


let () = Scanf.scanf "%s\n%s" f |> print_endline
