let id x = x

let n = Scanf.scanf "%d\n" id

let h = Hashtbl.create n

let ans = ref 0

let () =
  for i = 1 to n do
    let a = Scanf.scanf "%d " id in
    let v = try Hashtbl.find h a with _ -> 0 in
    Hashtbl.replace h a (v + 1)
  done ;
  Hashtbl.iter
    (fun k v ->
      if v < k then ans := !ans + v else if v > k then ans := !ans + (v - k))
    h ;
  Printf.printf "%d\n" !ans

