package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	in := strings.Split(sc.Text(), " ")
	a, _ := strconv.Atoi(in[0])
	b, _ := strconv.Atoi(in[1])
	c, _ := strconv.Atoi(in[2])
	d, _ := strconv.Atoi(in[3])

	if a+b > c+d {
		fmt.Println("Left")
	} else if a+b < c+d {
		fmt.Println("Right")
	} else {
		fmt.Println("Balanced")
	}
}
