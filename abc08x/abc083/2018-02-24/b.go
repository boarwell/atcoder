package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	in := strings.Split(sc.Text(), " ")
	n, _ := strconv.Atoi(in[0])
	a, _ := strconv.Atoi(in[1])
	b, _ := strconv.Atoi(in[2])

	// 各桁の和
	sum := 0
	// 数値を文字列に変換するための変数
	s := ""
	// 各桁の和が条件に合致する数値のスライス
	nums := make([]int, 0, n)

	for i := 1; i <= n; i++ {
		sum = 0
		s = strconv.Itoa(i)
		digits := strings.Split(s, "")
		for _, v := range digits {
			n, _ := strconv.Atoi(v)
			sum += n
		}
		if sum >= a && sum <= b {
			nums = append(nums, i)
		}
	}

	// 答えとなる和
	x := 0
	for _, v := range nums {
		x += v
	}
	fmt.Println(x)
}
