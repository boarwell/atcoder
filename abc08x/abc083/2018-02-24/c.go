package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	in := strings.Split(sc.Text(), " ")
	x, _ := strconv.Atoi(in[0])
	y, _ := strconv.Atoi(in[1])
	count := 0

	for x <= y {
		count++
		x *= 2
	}
	fmt.Println(count)
}
