let () =
  Scanf.scanf "%d %d %d %d" (fun a b c d ->
      if a + b < c + d then "Right"
      else if a + b > c + d then "Left"
      else "Balanced" )
  |> print_endline

