let sum_of_digits n =
  let rec aux n' tmp =
    if n' < 1 then tmp else aux (n' / 10) (tmp + n' mod 10)
  in
  aux n 0

let solve n a b =
  let rec aux i ans =
    if i > n then ans
    else match sum_of_digits i with
      | x when a <= x && x <= b -> aux (i + 1) (ans + i)
      | _ -> aux (i + 1) ans
  in
  aux 1 0

let () = Scanf.scanf "%d %d %d" solve |> Printf.printf "%d\n"

