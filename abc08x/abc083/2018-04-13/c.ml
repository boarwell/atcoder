let solve x y =
  let rec aux x' c = if x' > y then c else aux (x' * 2) (c + 1) in
  aux x 0

let () = Scanf.scanf "%d %d" solve |> Printf.printf "%d\n"

