package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	fline := strings.Split(sc.Text(), " ")
	a, _ := strconv.Atoi(fline[0])
	b, _ := strconv.Atoi(fline[1])
	sc.Scan()
	s := sc.Text()
	re := regexp.MustCompile(fmt.Sprintf("\\d{%d}-\\d{%d}", a, b))
	if re.MatchString(s) {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
