package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	lines := make([][2]int, 0, 100000)
	count := 0
	// 素数判定のメモのためのマップ
	m := map[int]bool{}

	sc.Scan()
	for sc.Scan() {
		line := strings.Split(sc.Text(), " ")
		l, _ := strconv.Atoi(line[0])
		r, _ := strconv.Atoi(line[1])
		lines = append(lines, [2]int{l, r})
	}

	var (
		yes             bool
		isMemorized     bool
		yesNext         bool
		isMemorizedNext bool
	)

	for _, v := range lines {
		count = 0
		for i := v[0]; i < v[1]+1; i++ {
			if i%2 == 0 {
				continue
			}
			// もしisMemorizedがfalseならyesはデフォルト値（false）が返る
			yes, isMemorized = m[i]
			if !isMemorized {
				yes = isPrime(i, m)
			}
			// iは奇数なので(i+1)/2は小数にならない
			yesNext, isMemorizedNext = m[(i+1)/2]
			if !isMemorizedNext {
				yesNext = isPrime((i+1)/2, m)
			}
			if yes && yesNext {
				count++
			}
		}
		fmt.Println(count)
	}
}

// mapは参照渡しのように振る舞うので引数に渡してももとのmapに変更が適用されます
// map, slice, chanはそういう感じです
// http://text.baldanders.info/golang/function-and-pointer/
func isPrime(n int, m map[int]bool) bool {
	yes, isMemorized := m[n]
	if isMemorized && yes {
		return true
	}

	// 2以外の偶数なら一発退場
	if n != 2 && n%2 == 0 {
		m[n] = false
		return false
	}

	// ここで1, 2を特別扱いしているのが気になる
	// もっとうまくやる方法が見つけられなかった
	if n == 1 {
		m[n] = false
		return false
	}
	if n == 2 {
		m[n] = true
		return true
	}

	// ここで単に i+2 としてしまうと
	// evaluated but not used というエラーが出る
	// https://stackoverflow.com/questions/39188542/for-loop-increment-with-a-step-not-working-in-golang
	for i := 3; i < n+1; i += 2 {
		if n == i {
			continue
		}
		if n%i == 0 {
			m[n] = false
			return false
		}
	}
	m[n] = true
	return true
}
