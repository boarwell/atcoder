let solve a s =
  let len = String.length s in
  if s.[a] <> '-' then "No"
  else
    let rec aux i =
      if i >= len then "Yes"
      else if (s.[i] <> '-') || i = a then aux (i + 1)
      else "No"
    in
    aux 0


let () = Scanf.scanf "%d %d\n%s" (fun a _ s -> solve a s) |> print_endline
