let n = Scanf.scanf "%d\n" (fun x -> x)

let ar =
  Array.init (n - 1) (fun _ ->
      Scanf.scanf "%d %d %d\n" (fun c s f -> (c, s, f)) )


let () =
  for i = 0 to n - 2 do
    let c, s, f = ar.(i) in
    let tmp = ref (c + s) in
    for j = i + 1 to n - 2 do
      let nc, ns, nf = ar.(j) in
      if ns < !tmp then
        let ns' =
          if !tmp mod nf = 0 then !tmp else !tmp + nf - !tmp mod nf
        in
        tmp := ns' + nc
      else tmp := ns + nc
    done ;
    Printf.printf "%d\n" !tmp
  done ;
  Printf.printf "%d\n" 0

