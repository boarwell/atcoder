package main

import (
	"bufio"
	"os"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	println(strings.Replace(sc.Text(), "2017", "2018", -1))
}
