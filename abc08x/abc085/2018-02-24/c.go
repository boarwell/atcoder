package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	in := strings.Split(sc.Text(), " ")

	bills, _ := strconv.Atoi(in[0])
	price, _ := strconv.Atoi(in[1])

	for man := 0; man <= bills; man++ {
		for gosen := 0; gosen <= bills; gosen++ {
			for sen := 0; sen <= bills; sen++ {
				if man*10000+gosen*5000+sen*1000 == price {
					if man+gosen+sen == bills {
						fmt.Println(man, gosen, sen)
						return
					}
				}
			}
		}
	}
	fmt.Println(-1, -1, -1)

}
