package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)

	sc.Scan()
	in := strings.Split(sc.Text(), " ")
	howManyKatanas, _ := strconv.Atoi(in[0])
	hp, _ := strconv.Atoi(in[1])

	// [0] -> 振ったとき
	// [1] -> 投げたとき
	katanas := make([][2]int, howManyKatanas)
	throwed := make([]bool, howManyKatanas)

	i := 0
	for sc.Scan() {
		line := strings.Split(sc.Text(), " ")
		katanas[i][0], _ = strconv.Atoi(line[0])
		katanas[i][1], _ = strconv.Atoi(line[1])
		i++
	}

	// 累計ダメージ
	damage := 0
	// 何回攻撃したか
	turn := 0
	for damage < hp {
		turn++
		// 攻撃する刀の攻撃力
		power := 0
		// どの刀を投げるかのインデックス
		whichKatana := 0
		for i, v := range katanas {
			if !throwed[i] {
				if v[1] > power {
					power = v[1]
					whichKatana = i
				}
			} else {
				if v[0] > power {
					power = v[0]
				}
			}
		}
		throwed[whichKatana] = true
		damage += power
		println(damage)
	}
	println(turn)

}
