let n, y = Scanf.scanf "%d %d" (fun a b -> (a, b))

let calc tt ft =
  let t = n - tt - ft in
  if tt * 10000 + ft * 5000 + t * 1000 = y then Some (tt, ft, t) else None


let rec inner tt =
  let rec aux ft =
    if tt + ft > n then None
    else match calc tt ft with Some x -> Some x | None -> aux (ft + 1)
  in
  aux 0


let rec main tt =
  if tt > n then (-1, -1, -1)
  else match inner tt with Some x -> x | None -> main (tt + 1)


let p (tt, ft, t) = Printf.printf "%d %d %d\n" tt ft t

let () = main 0 |> p
