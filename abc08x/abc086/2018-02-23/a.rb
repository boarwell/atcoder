a, b = gets.split.map(&:to_i)

if a.even? || b.even?
  puts 'Even'
else
  puts 'Odd'
end