package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func getInputC() [][3]int {
	in := make([][3]int, 0, 100000)

	sc := bufio.NewScanner(os.Stdin)

	// 最初の行は行数なので無視します
	sc.Scan()

	for sc.Scan() {
		s := strings.Split(sc.Text(), " ")
		l := [3]int{}
		for i, v := range s {
			n, _ := strconv.Atoi(v)
			l[i] = n
		}
		in = append(in, l)
	}

	return in

}

func main() {
	start := [][2]int{[2]int{0, 0}}
	in := getInputC()

	for _, v := range in {
		x := getThePositionsInTheTime(start, 0, v[0])
		target := [2]int{v[1], v[2]}

		if !contains(x, target) {
			fmt.Println("No")
			return
		}
	}

	fmt.Println("Yes")
}

func getThePositionsInTheTime(start [][2]int, now, when int) [][2]int {
	nextPositions := make([][2]int, 0, len(start)*4)

	if now == when {
		return start
	}

	for _, v := range start {

		for i := 0; i < 4; i++ {
			switch i {
			case 0:
				// ここで単にv[0]++としてしまうとなぜか正しく動作しなかった
				w := [2]int{v[0] + 1, v[1]}
				nextPositions = append(nextPositions, w)
			case 1:
				w := [2]int{v[0] - 1, v[1]}
				nextPositions = append(nextPositions, w)
			case 2:
				w := [2]int{v[0], v[1] + 1}
				nextPositions = append(nextPositions, w)
			case 3:
				w := [2]int{v[0], v[1] - 1}
				nextPositions = append(nextPositions, w)
			}
		}
	}

	// 毎回重複を取り除いておかないと計算量がとんでもないことになります（なりました）
	nextPositions = removeDup(nextPositions)
	// ここで再帰を使えたのがうれしい！
	// 自分で考えたんだよ！！
	return getThePositionsInTheTime(nextPositions, now+1, when)
}

// 残念ながらこれはQiitaの記事を参考に実装しました
// つぎに重複削除が必要になったときは自分で実装できるようにしましょう
// https://qiita.com/hi-nakamura/items/5671eae147ffa68c4466
func removeDup(args [][2]int) [][2]int {
	results := make([][2]int, 0, len(args))
	encountered := map[string]bool{}

	for _, v := range args {
		if !encountered[fmt.Sprint(v)] {
			encountered[fmt.Sprint(v)] = true
			results = append(results, v)
		}
	}

	return results
}

func contains(list [][2]int, target [2]int) bool {
	for _, v := range list {
		if v == target {
			return true
		}
	}
	return false
}
