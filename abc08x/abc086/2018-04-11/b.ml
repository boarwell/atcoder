let cat a b = float_of_string (a ^ b)

let is_square n =
  let root = sqrt n |> floor in n = (root ** 2.)

let p b = (if b then "Yes" else "No") |> print_endline

let () =
  Scanf.scanf "%s %s" cat |> is_square |> p
