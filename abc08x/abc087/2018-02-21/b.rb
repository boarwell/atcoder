tmp = []

STDIN.each_line do |line|
  tmp << line.slice(/\d+/)
end

a = tmp[0].to_i
b = tmp[1].to_i
c = tmp[2].to_i
x = tmp[3].to_i

ans = 0

0.upto(a) do |i|
  0.upto(b) do |j|
    0.upto(c) do |k|
      res = i * 500 + j * 100 + k * 50
      ans += 1 if res == x
    end
  end
end

puts ans
