let () =
  Scanf.scanf "%d\n%d\n%d" (fun x a b -> (x - a) mod b)
  |> Printf.printf "%d\n"