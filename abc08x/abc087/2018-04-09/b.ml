let a, b, c, x = Scanf.scanf "%d\n%d\n%d\n%d" (fun a b c x -> (a, b, c, x))

let rec calc a' b' tmp =
  let rec aux c' tmp' =
    if c' > c then tmp'
    else if a' * 500 + b' * 100 + c' * 50 = x then aux (c' + 1) (tmp' + 1)
    else aux (c' + 1) tmp'
  in
  aux 0 tmp


let rec secound a' tmp =
  let rec aux b' tmp' =
    if b' > b then tmp'
    else
      let tmp'' = calc a' b' tmp' in
      aux (b' + 1) tmp''
  in
  aux 0 tmp


let rec main a' tmp =
  if a' > a then tmp
  else
    let tmp' = secound a' tmp in
    main (a' + 1) tmp'


let () = main 0 0 |> Printf.printf "%d\n"
