let id x = x

let init i = if i = 0 then 0 else Scanf.scanf "%d " id

let n = Scanf.scanf "%d\n" id

let row1 = Array.init (n + 1) init

let row2 = Array.init (n + 1) init

let rec solve x y tmp =
  if x = 0 then tmp
  else
    let upper = Array.fold_left ( + ) 0 (Array.sub row1 1 x)
    and lower = Array.fold_left ( + ) 0 (Array.sub row2 y (n - y + 1)) in
    solve (x - 1) (y - 1) (max tmp (upper + lower))


let () = solve n n 0 |> Printf.printf "%d\n"
