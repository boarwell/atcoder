package main

import (
	"fmt"
	"sort"
)

// 配列を降順ソートして頭からalice, bobに足していく

func main() {
	var n int
	fmt.Scanln(&n)

	a := make([]int, 0, n)
	var tmp int

	for i := 0; i < n; i++ {
		// tmpに代入 -> appendではなく
		// rangeでa[i]=vとやろうとするとindex out of rangeで死にます
		fmt.Scan(&tmp)
		a = append(a, tmp)
	}

	sort.Sort(sort.Reverse(sort.IntSlice(a)))

	var alice, bob int

	for i, v := range a {
		// 0 % 2 という演算でもエラーはでないらしい
		if i%2 == 0 {
			alice += v
		} else {
			bob += v
		}
	}

	fmt.Println(alice - bob)
}
