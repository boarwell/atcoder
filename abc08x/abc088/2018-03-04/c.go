package main

import (
	"fmt"
	"os"
)

func main() {
	c := [3][3]int{}
	// 配列だとすでに0で初期化されているため、
	// fmt.Scanln(&c[i][0], &c[i][1], &c[i][2])ができる
	for i := 0; i < 3; i++ {
		fmt.Scanln(&c[i][0], &c[i][1], &c[i][2])
	}
	// 前回の結果を保存するための変数
	var a1a2, a2a3, b1b2, b2b3 int

	for i := 0; i < 3; i++ {
		// ループの初回は比較できないため代入のみ
		if i == 0 {
			a1a2 = c[0][i] - c[1][i]
			a2a3 = c[1][i] - c[2][i]
			b1b2 = c[i][0] - c[i][1]
			b2b3 = c[i][1] - c[i][2]
			continue
		}

		if a1a2 != c[0][i]-c[1][i] || a2a3 != c[1][i]-c[2][i] {
			fmt.Println("No")
			os.Exit(0)
		}
		if b1b2 != c[i][0]-c[i][1] || b2b3 != c[i][1]-c[i][2] {
			fmt.Println("No")
			os.Exit(0)
		}

		a1a2 = c[0][i] - c[1][i]
		a2a3 = c[1][i] - c[2][i]
		b1b2 = c[i][0] - c[i][1]
		b2b3 = c[i][1] - c[i][2]
	}
	fmt.Println("Yes")
}
