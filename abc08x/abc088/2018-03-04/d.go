package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var (
	h, w  int
	m     [][]string
	q     [][2]int
	next  [2]int
	max   int
	board [][]int
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	fline := strings.Split(sc.Text(), " ")
	h, _ = strconv.Atoi(fline[0])
	w, _ = strconv.Atoi(fline[1])

	for sc.Scan() {
		m = append(m, strings.Split(sc.Text(), ""))
	}

	for i := 0; i < h; i++ {
		tmp := make([]int, 0, w)
		for j := 0; j < w; j++ {
			tmp = append(tmp, -1)
		}
		board = append(board, tmp)
	}

	board[0][0] = 0

	for len(q) > 0 {

	}
}

func search(pos [2]int) {
	if board[pos[0]][pos[1]] != -1 {
		return
	}
	fmt.Println("search", pos)
	// 右端の列にいた場合
	if pos[1] == w-1 {
		// 一番下の行ではない場合
		if pos[0] != h-1 {
			// 下のみを探索キューに入れる
			if m[pos[0]+1][pos[1]] != "#" {
				fmt.Println("migihashi!")
				q = append(q, [2]int{pos[0] + 1, pos[1]})
			}
		}
		// 一番下の行にいた場合
	} else if pos[0] == h-1 {
		// 右端の列ではない場合
		if pos[1] != w-1 {
			// 右のみを探索キューに入れる
			if m[pos[0]][pos[1]+1] != "#" {
				fmt.Println("shita!")
				q = append(q, [2]int{pos[0], pos[1] + 1})

			}
		}
	} else {
		if m[pos[0]+1][pos[1]] != "#" {
			fmt.Println("add", pos[0]+1, pos[1])
			q = append(q, [2]int{pos[0] + 1, pos[1]})
		}
		if m[pos[0]][pos[1]+1] != "#" {
			fmt.Println("add", pos[0], pos[1]+1)
			q = append(q, [2]int{pos[0], pos[1] + 1})
		}
	}
}

func dequeue() [2]int {
	x := q[0]
	q = q[1:]
	return x
}

func howManeHashes() int {
	var n int
	for _, v := range m {
		for _, w := range v {
			if w == "#" {
				n++
			}
		}
	}
	return n
}
