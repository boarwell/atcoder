package old

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var (
	h, w    int
	m       [][]string
	q       [][2]int
	next    [2]int
	count   int
	visited = map[[2]int]bool{}
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	fline := strings.Split(sc.Text(), " ")
	h, _ = strconv.Atoi(fline[0])
	w, _ = strconv.Atoi(fline[1])

	for sc.Scan() {
		m = append(m, strings.Split(sc.Text(), ""))
	}

	search([2]int{0, 0})
	for len(q) > 0 {
		count++
		next = dequeue()
		if next == [2]int{h - 1, w - 1} {
			fmt.Println(
				// 全部のマスの数から#のところを引いた数が白いところ
				// そのうちcountは黒にできない
				(h * w), howManeHashes(), count,
			)
			os.Exit(0)
		}
		search(next)
	}
	// たどり着けなかった場合
	fmt.Println(-1)
}

func howManeHashes() int {
	var n int
	for _, v := range m {
		for _, w := range v {
			if w == "#" {
				n++
			}
		}
	}
	return n
}

func search(pos [2]int) {
	if visited[pos] {
		return
	}
	visited[pos] = true
	fmt.Println("search", pos)
	// 右端の列にいた場合
	if pos[1] == w-1 {
		// 一番下の行ではない場合
		if pos[0] != h-1 {
			// 下のみを探索キューに入れる
			if m[pos[0]+1][pos[1]] != "#" {
				fmt.Println("migihashi!")
				q = append(q, [2]int{pos[0] + 1, pos[1]})
			}
		}
		// 一番下の行にいた場合
	} else if pos[0] == h-1 {
		// 右端の列ではない場合
		if pos[1] != w-1 {
			// 右のみを探索キューに入れる
			if m[pos[0]][pos[1]+1] != "#" {
				fmt.Println("shita!")
				q = append(q, [2]int{pos[0], pos[1] + 1})

			}
		}
	} else {
		if m[pos[0]+1][pos[1]] != "#" {
			fmt.Println("add", pos[0]+1, pos[1])
			q = append(q, [2]int{pos[0] + 1, pos[1]})
		}
		if m[pos[0]][pos[1]+1] != "#" {
			fmt.Println("add", pos[0], pos[1]+1)
			q = append(q, [2]int{pos[0], pos[1] + 1})
		}
	}
}

func dequeue() [2]int {
	x := q[0]
	q = q[1:]
	return x
}
