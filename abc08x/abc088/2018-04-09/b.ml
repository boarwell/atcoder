let id x = x

let mk_ls n =
  let rec aux i ls =
    if i = 0 then List.fast_sort (fun i j -> j - i) ls
    else aux (i - 1) ((Scanf.scanf "%d " id) :: ls)
  in
  aux n []

let solve ls =
  let rec aux i a b = function
    | [] -> a - b
    | h :: t ->
      let i' = i + 1 in
      if i mod 2 = 1 then aux i' (a + h) b t else aux i' a (b + h) t
  in
  aux 1 0 0 ls

let () =
  mk_ls (Scanf.scanf "%d\n" id) |> solve |> Printf.printf "%d\n"