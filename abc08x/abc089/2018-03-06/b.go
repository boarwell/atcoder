package main

import (
	"fmt"
	"os"
)

func main() {
	// 入力
	var n int
	fmt.Scan(&n)

	s := make([]string, 0, n)
	var tmp string
	for i := 0; i < n; i++ {
		fmt.Scan(&tmp)
		s = append(s, tmp)
	}

	// 処理
	for _, v := range s {
		if v == "Y" {
			fmt.Println("Four")
			os.Exit(0)
		}
	}

	// 出力
	fmt.Println("Three")
}
