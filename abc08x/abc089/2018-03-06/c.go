package main

import (
	"bufio"
	"fmt"
	"os"
)

var (
	initial = [5]string{"M", "A", "R", "C", "H"}
	m       = map[string]int{}
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	for sc.Scan() {
		switch string(sc.Text()[0]) {
		case "M", "A", "R", "C", "H":
			m[string(sc.Text()[0])]++
		}
	}

	fmt.Println(combination())
}

func combination() int {
	var count int

	for i := 0; i < len(initial)-2; i++ {
		for j := i + 1; j < len(initial)-1; j++ {
			for k := j + 1; k < len(initial); k++ {
				count += m[initial[i]] * m[initial[j]] * m[initial[k]]
			}
		}
	}

	return count
}
