initial = %w[M A R C H]
m = { 'M' => 0, 'A' => 0, 'R' => 0, 'C' => 0, 'H' => 0 }
count = 0

while line = gets
  m[line.chr] += 1 if initial.include?(line.chr)
end

0.upto(2) do |i|
  (i + 1).upto(3) do |j|
    (j + 1).upto(4) do |k|
      count += m[initial[i]] * m[initial[j]] * m[initial[k]]
    end
  end
end

puts count
