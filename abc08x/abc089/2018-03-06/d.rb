h, w, d = gets.split.map(&:to_i)
a = {}

1.upto(h) do |i|
  line = gets.split.map(&:to_i)
  line.each_with_index do |v, j|
    # 0-indexedなので+1
    a[v] = [i, j + 1]
  end
end

q = gets.to_i

lr = []
q.times do
  lr.push gets.split.map(&:to_i)
end

cost = Hash.new(1)

(d + 1).upto(h * w) do |i|
  cost[i] = cost[i - d] + (a[i][0] - a[i - d][0]).abs +
            (a[i][1] - a[i - d][1]).abs
end

lr.each do |x|
  puts cost[x[1]] - cost[x[0]]
end
