package main

import (
	"fmt"
)

func main() {
	// 入力
	var n int
	fmt.Scanln(&n)

	s := make([]string, 0, n)
	var tmp string

	for i := 0; i < n; i++ {
		fmt.Scanln(&tmp)
		// tmp[0]はstring()でくくらないと[]byteになってしまう
		switch string(tmp[0]) {
		case "M", "A", "R", "C", "H":
			s = append(s, tmp)
		}
	}

	// 出力
	fmt.Println(bfs(s))
}

// 入力の最大長は10**5なので全探索したら死にます（死にました）
func bfs(s []string) int {
	if len(s) < 3 {
		return 0
	}

	var count int

	for i := 0; i < len(s)-2; i++ {
		for j := i + 1; j < len(s)-1; j++ {
			for k := j + 1; k < len(s); k++ {
				if nodup(s[i], s[j], s[k]) {
					count++
				}
			}
		}
	}

	return count
}

func nodup(a, b, c string) bool {
	if a[0] == b[0] || b[0] == c[0] || c[0] == a[0] {
		return false
	}
	return true
}
