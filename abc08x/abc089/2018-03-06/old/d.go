// 自分で実装したコード
// アルゴリズム的には間違っていないと思うが、
// いじわるな入力でTLEになって死んだ

package main

import (
	"fmt"
)

var (
	h, w, d int
	m       = make(map[int][2]int) // Aのためのマップ
	x       int                    // Scanのために使う一時変数
	q       int
	l, r    int
	lr      = make([][2]int, 0, q)
	mp      int
)

func main() {
	// 入力
	fmt.Scanln(&h, &w, &d)
	for i := 0; i < h; i++ {
		for j := 0; j < w; j++ {
			fmt.Scan(&x)
			m[x] = [2]int{i + 1, j + 1}
		}
	}

	fmt.Scan(&q)
	for i := 0; i < q; i++ {
		fmt.Scanln(&l, &r)
		lr = append(lr, [2]int{l, r})
	}

	for _, v := range lr {
		mp = 0
		// わかりやすさのために変数に入れます
		l = v[0]
		r = v[1]

		for l < r {
			mp += abs(m[l][0]-m[l+d][0]) + abs(m[l][1]-m[l+d][1])
			l += d
		}

		fmt.Println(mp)
	}
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
