let rec f n =
  if n = 0 then "Three"
  else match Scanf.scanf "%s " (fun s -> s) with
    | "Y" -> "Four"
    | _ -> f (n - 1)

let () = Scanf.scanf "%d\n" f |> print_endline
