package main

import "fmt"

var (
	a, b  int
	count int
)

func main() {
	fmt.Scan(&a, &b)
	for i := a; i <= b; i++ {
		if isPalindorome(i) {
			count++
		}
	}

	fmt.Println(count)
}

func isPalindorome(x int) bool {
	if x/10000 == x%10 {
		y := (x - ((x / 10000) * 10000)) / 10
		if y/100 == y%10 {
			return true
		}
	}

	return false
}
