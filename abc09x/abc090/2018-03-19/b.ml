let is_palindrome n =
  if n / 10000 = n mod 10 then
    let m = (n - 10000 * (n / 10000)) / 10 in
    if m / 100 = m mod 10 then true else false
  else false


let f a b =
  let rec g a' count =
    if a' > b then count
    else if is_palindrome a' then g (a' + 1) (count + 1)
    else g (a' + 1) count
  in
  g a 0


let () =
  (*
    - a, bに束縛する必要ない
    - Scanf.scanに渡す関数内で計算を行える
    というリファクタリングができました。
  *)
  Scanf.scanf "%d %d" (fun a b -> f a b)
  |> Printf.printf "%d\n"
