package main

import "fmt"

var (
	n, m int
)

func main() {
	fmt.Scan(&n, &m)

	fmt.Println(abs((n * m) - (2 * m) - (2 * n) + 4))
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
