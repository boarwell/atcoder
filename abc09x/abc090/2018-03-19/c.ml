let () =
  Scanf.scanf "%d %d" (fun n m -> 
      abs (n * m - 2 * n - 2 * m + 4))
  |> Printf.printf "%d\n"
