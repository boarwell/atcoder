let () =
  Array.init 3 (fun _ -> read_line ())
  |> Array.iteri (fun i s -> print_char s.[i]) ; print_newline ()