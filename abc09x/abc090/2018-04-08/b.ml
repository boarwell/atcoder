let is_palindrome n =
  let d = n / 10000 in
  if d = n mod 10 then
    let m = (n - d * 10000) / 10 in m / 100 = m mod 10
  else false

let solve s f =
  let rec aux x sum =
    if x > f then sum
    else if is_palindrome x then aux (x + 1) (sum + 1)
    else aux (x + 1) sum
  in
  aux s 0

let () = Scanf.scanf "%d %d" solve |> Printf.printf "%d\n"