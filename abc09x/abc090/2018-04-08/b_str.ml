let is_palindrome s = s.[0] = s.[4] && s.[1] = s.[3]

let inc_str s = int_of_string s + 1 |> string_of_int

let solve start fin =
  let rec aux x sum =
    if int_of_string x > int_of_string fin then sum
    else if is_palindrome x then aux (inc_str x) (sum + 1)
    else aux (inc_str x) sum
  in
  aux start 0

let () = Scanf.scanf "%s %s" solve |> Printf.printf "%d\n"