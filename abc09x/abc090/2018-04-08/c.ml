let () =
  Scanf.scanf "%d %d" (fun n m -> n * m - (2 * n + 2 * m - 4))
  |> abs |> Printf.printf "%d\n"