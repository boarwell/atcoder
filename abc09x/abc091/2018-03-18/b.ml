let inc_hash_value h k =
  let v = try Hashtbl.find h k with Not_found -> 0 in
  Hashtbl.replace h k (v + 1)


let rec init_hash n h =
  if n = 0 then () else inc_hash_value h (read_line ()) ;
  init_hash (n - 1) h


let () =
  let n = read_int () in
  let blue = Hashtbl.create n in
  let () = init_hash n blue in
  Hashtbl.iter (Printf.printf "%s: %d\n") blue;
