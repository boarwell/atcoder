// AtCoderのGoは1.6で、ここで使っているsort.Sliceは1.8かららしいので
// 提出するとコンパイルエラーになります

package main

import (
	"fmt"
	"sort"
)

var (
	n    int
	a, b int
	c, d int
)

func main() {
	fmt.Scan(&n)
	red := make([][]int, 0, n)
	blue := make([][]int, 0, n)

	for i := 0; i < n; i++ {
		fmt.Scan(&a, &b)
		red = append(red, []int{a, b})
	}
	for i := 0; i < n; i++ {
		fmt.Scan(&c, &d)
		blue = append(blue, []int{c, d})
	}

	// 二次元配列のソート
	// https://stackoverflow.com/questions/42629541/go-lang-sort-a-2d-array
	sort.Slice(blue, func(i, j int) bool {
		return blue[i][0] < blue[j][0]
	})

	count := 0
	for _, v := range blue {
		// 現在見ている青の点より左側にある赤い点を列挙する
		candidate := filter(red, v[0])
		// yの降順でソート
		sort.Slice(candidate, func(i, j int) bool {
			return candidate[i][1] > candidate[j][1]
		})
		// ペア候補の赤い点の中からy座標が一番大きいものを選んでペアを作る
		// ペアを作った赤い点は削除する
		for _, w := range candidate {
			if w[1] < v[1] {
				red = del(red, w)
				count++
				break
			}
		}
	}
	fmt.Println(count)

}

// del は赤い点の集合からペアを作った点を削除する
func del(red [][]int, where []int) [][]int {
	ret := make([][]int, 0, len(red))
	for _, v := range red {
		if v[0] == where[0] && v[1] == where[1] {
			continue
		}

		ret = append(ret, v)
	}

	return ret
}

// filter は赤い点の集合から、現在見ている青い点より左側にあるものを抽出する
func filter(red [][]int, blueX int) [][]int {
	ret := make([][]int, 0, len(red))
	for _, v := range red {
		if v[0] < blueX {
			ret = append(ret, v)
		}
	}

	return ret
}
