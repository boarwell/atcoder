n = gets.to_i
red = n.times.map { gets.split.map(&:to_i) }.sort_by { |a| a[0] }
blue = n.times.map { gets.split.map(&:to_i) }.sort_by { |a| a[0] }

pair = 0
blue.each do |b|
  candidate = red.select { |rs| rs[0] < b[0] }.sort_by { |r| r[1] }.reverse
  candidate.each do |c|
    next unless c[1] < b[1]
    pair += 1
    red.delete c
    break
  end
end

puts pair
