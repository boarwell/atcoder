n = gets.to_i
red = []

n.times do
  red.push gets.split.map(&:to_i)
end

blue = []

n.times do
  blue.push gets.split.map(&:to_i)
end

red.sort! { |a, b| [a[0], a[1]].min <=> [b[0], b[1]].min }
blue.sort! { |a, b| [a[0], a[1]].min <=> [b[0], b[1]].min }

count = 0

red.product(blue).collect do |set|
  count += 1 if set[0][0] < set[1][0] && set[0][1] < set [1][1]
end

puts count
