# ABC091

2018-04-08

## テストケース


## A - Two Coins

- 単純にa+bがc以上かどうかを判定

## B - Two Colors Card Game

- 前回やったときはOCamlでハッシュテーブルが使えなかったみたいですが、今回はばっちり実装できました
  - 成長ポイント！

## C - 2D Plane 2N Points

- 青い点の集合はx座標でソート
- 青い点の集合でイテレート
- 現在の青い点のx座標より小さいx座標を持つ赤い点の集合を作成
- その中からもっとも大きいy座標を持つ赤い点とペアにする

- なんとかOCamlで実装できた
  - パターンマッチがうまく使えると気持ちいい

- この問題で重要なのはペアの作り方だと思います
  - 一応この問題での考え方は理解しましたが、同じような問題が出たときに自分で解けるかどうか不安です
