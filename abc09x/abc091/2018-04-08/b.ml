let rec init_hash i h =
  if i = 0 then ()
  else
    let s = read_line () in
    let v = try Hashtbl.find h s with _ -> 0 in
    Hashtbl.replace h s (v + 1) ;
    init_hash (i - 1) h

let n = read_int ()
let b = Hashtbl.create n
let () = init_hash n b

let m = read_int ()
let r = Hashtbl.create m
let () = init_hash m r

let () =
  Hashtbl.fold (fun k v tmp ->
      let red = try Hashtbl.find r k with _ -> 0 in 
      max tmp (v - red))
    b 0
  |> Printf.printf "%d\n"

