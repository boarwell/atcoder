let id x = x
let n = Scanf.scanf "%d\n" id
let d, x = Scanf.scanf "%d %d\n" (fun a b -> a, b)

let eat freq =
  let rec aux day sum =
    if d < day then sum else aux (day + freq) (sum + 1)
  in
  aux 1 0

let solve n = 
  let rec aux i sum =
    if i = 0 then sum
    else aux (i - 1) (sum + eat (Scanf.scanf "%d\n" id))
  in
  aux n 0

let () = solve n |> (+) x |> Printf.printf "%d\n"