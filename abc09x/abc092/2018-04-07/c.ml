let id x = x
let n = Scanf.scanf "%d\n" id
let ar = Array.make (n+2) 0 (*先頭と末尾に0を入れたいのでn+1*)

let sum_init i =
  let rec sum i' tmp =
    if i' = (n + 1) then tmp + abs ar.(0) + abs ar.(n)
    else
      let v = Scanf.scanf "%d " id in
      let () = ar.(i') <- v in
      sum (i' + 1) (tmp + abs (v - ar.(i' - 1)))
  in
  sum 1 0

let cost = sum_init n

let skip i =
  (*先頭と末尾に0を入れているのですべて同じ式で値を出せる*)
  let off = abs (ar.(i) - ar.(i-1)) + abs (ar.(i + 1) - ar.(i))
  and add = abs (ar.(i + 1) - ar.(i - 1)) in
  cost - off + add


let () = for i = 1 to n do skip i |> Printf.printf "%d\n" done
