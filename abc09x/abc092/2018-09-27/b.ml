let () =
  let n = Scanf.scanf "%d\n" (fun a -> a) in
  let d, x = Scanf.scanf "%d %d\n" (fun a b -> (a, b)) in
  let f n =
    let rec aux i acc =
      if i > n then acc
      else
        let a = Scanf.scanf "%d\n" (fun a -> a) in
        let m = if d mod a = 0 then 0 else 1 in
        aux (i + 1) (acc + (d / a) + m)
    in
    aux 1 0
  in
  Printf.printf "%d\n" (f n + x)
