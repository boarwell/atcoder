a, b, k = gets.split.map(&:to_i)

ar = (a..b).to_a

ar.each_with_index do |v, i|
  if ar.length - i <= k || i < k
    p v
  end
end