ar = gets.split.map(&:to_i).sort

x = ar[2] - ar[0]
y = ar[2] - ar[1]

if x.even? && y.even?
  p (x / 2) + (y / 2)
elsif x.even? && y.odd?
  p (x / 2) + (y / 2 + 1) + 1
elsif x.odd? && y.odd?
  x -= 1
  y -= 1
  p (x / 2) + (y / 2) + 1
else
  p (y / 2) + (x / 2 + 1) + 1
end
