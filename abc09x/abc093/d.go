package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	sc.Scan()
	q := s2i(sc.Text())
	ls := make([][]int, 0, q)
	for i := 0; i < q-1; i++ {
		tmp := make([]int, 2)
		sc.Scan()
		tmp[0] = s2i(sc.Text())
		sc.Scan()
		tmp[1] = s2i(sc.Text())
		ls = append(ls, tmp)
	}

	fmt.Println(ls)

	for _, v := range ls {
		fst := make([]bool, 1000000000)
		snd := make([]bool, 1000000000)
		fst[v[0]] = true
		snd[v[1]] = true
		tscore := v[0] * v[1]

		count := 0
		for i := 1; i < tscore; i++ {
			var sscore int
			if tscore%i == 0 {
				sscore = tscore/i + 1
			} else {
				sscore = tscore / 1
			}

			if fst[i] || snd[sscore] {
				continue
			}

			count++
			fst[i] = true
			snd[sscore] = true
		}
		fmt.Println(count)
	}
}

func s2i(s string) int {
	i, _ := strconv.Atoi(s)
	return i
}
