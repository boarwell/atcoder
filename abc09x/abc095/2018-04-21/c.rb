a, b, c, x, y = gets.split.map(&:to_i)
d = a * x + b * y
all = c * [x,y].max * 2
mi = ma = k = 0

if x < y
  mi = x
  ma = y
  k = c * mi * 2 + (ma - mi) * b
else
  mi = y
  ma = x
  k = c * mi * 2 + (ma - mi) * a
end

p [d, all, k].min