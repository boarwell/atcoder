n, c = gets.split.map(&:to_i)
arr = []
h = {}

n.times do
  x, v = gets.split.map(&:to_i)
  arr << x
  h[x] = v
end

now = ans = 0
edge = false

until arr.empty?
  rd = arr[0] - now
  ld = now + (c - arr[-1])
  printf("now: %d ans: %d, %d %d\n", now, ans, ld, rd)
  if rd <= ld && h[arr[0]] - rd >= 0
    p 'right plus'
    now = arr[0]
    ans += h[arr[0]] - rd
    arr.delete_at(0)
  elsif rd <= ld && h[arr[0]] - rd < 0
    break if h[arr[-1]] - rd < 0
    now = arr[-1]
    ans += h[arr[-1]] - ld
    arr.delete_at(-1)
  elsif rd > ld && h[arr[-1]] - ld >= 0
    now = arr[-1]
    ans += h[arr[-1]] - ld
    arr.delete_at(-1)
  else
    break if h[arr[0]] - rd < 0
    now = arr[0]
    ans += h[arr[0]] - rd
    arr.delete_at(0)
  end
end

p ans