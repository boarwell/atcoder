require 'set'

s = gets.chomp
k = gets.to_i

set = Set.new
len = s.length

0.upto(len - 1) do |i|
  1.upto(len - i) do |j|
    break if j > k
    set.add s[i, j]
  end
end

puts set.to_a.sort[k-1]
