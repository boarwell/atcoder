# frozen_string_literal: true

x = gets.to_i
root = (x**0.5).floor
ans = root**2

tmp = 1

root.downto(2) do |i|
  tmp = 1

  tmp *= i while tmp <= x

  ans = (tmp / i) if ans < (tmp / i)
  ans = tmp if tmp == x
end

p ans
