# frozen_string_literal: true

require 'set'

s = gets.chomp
k = gets.to_i

set = Set.new
len = s.length

0.upto(len - 1) do |st|
  1.upto(k) do |le|
    break if st + le >= len
    set.add s[st, le]
  end
end

p set.to_a.sort
