# frozen_string_literal: true

a, b = gets.split.map(&:to_i)
diff = b - a

tmp = 0
1.upto(diff) do |i|
  tmp += i
end

p(tmp - b)
