# frozen_string_literal: true

n = gets.to_i
nia = [0]
ni = 9
sia = [0]
si = 6

while ni < n
  nia.push ni
  ni *= 9
end

while si < n
  sia.push si
  si *= 6
end

tmp = 100_000
nia.each_with_index do |nine, ninei|
  sia.each_with_index do |six, sixi|
    rest = n - nine - six
    tmp = [(rest + ninei + sixi), tmp].min
  end
end

p tmp
