gets
a = gets.split.map(&:to_i)

ans = 0
a.each do |ai|
  while ai.even?
    ai /= 2
    ans += 1
  end
end

p ans
