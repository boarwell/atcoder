// まったくわからないので
// とりあえず全探索だけしてみます

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var (
	n, m  int
	max   int
	cakes [][]int
)

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func calc(ls []int) {
	var (
		tmpb, tmpt, tmpp int
	)
	for _, i := range ls {
		cake := cakes[i]
		tmpb += cake[0]
		tmpt += cake[1]
		tmpp += cake[2]
	}
	tmp := abs(tmpb) + abs(tmpt) + abs(tmpp)
	if tmp > max {
		max = tmp
	}
}

func search(i int, tmp []int) {
	if len(tmp) == m {
		calc(tmp)
		return
	} else if i >= n {
		return
	}

	search(i+1, append(tmp, i))
	search(i+1, tmp)
}

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	nm := strings.Split(sc.Text(), " ")

	n, _ = strconv.Atoi(nm[0])
	m, _ = strconv.Atoi(nm[1])

	for i := 0; i < n; i++ {
		sc.Scan()
		line := strings.Split(sc.Text(), " ")
		b, _ := strconv.Atoi(line[0])
		t, _ := strconv.Atoi(line[1])
		p, _ := strconv.Atoi(line[2])
		cakes = append(cakes, []int{b, t, p})
	}

	search(0, []int{})
	fmt.Println(max)
}
