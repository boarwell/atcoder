let s = read_line ()

let () =
  let hd = String.sub s 0 2 in 
  let tl = String.sub s 2 2 in
  (if hd = tl then "Yes" else "No") |> print_endline